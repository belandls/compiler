﻿using Compiler;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompilerDriver
{
    class Program
    {
        static void Main(string[] args)
        {

            if (args.Count() < 1)
            {
                Console.WriteLine("You must specify the filename as the first argument.");
            }
            else
            {
                string outName;
                if(args.Count() == 2)
                {
                    outName = args[1];
                }
                else
                {
                    outName = "out.m";
                }
                string filename = args[0];
                FileIO fio = new FileIO();
                try
                {
                    fio.LoadFile(filename);
                    Lexer l = new Lexer(fio);
                    var tokens = l.ExtractTokens();

                    SyntaxAnalyser sa = new SyntaxAnalyser(outName);
                    bool parseSuccesful = sa.Parse(tokens[0]);
                    

                    using (var validFile = new StreamWriter("valid.txt",false))
                    {
                        using (var invalidFile = new StreamWriter("invalid.txt", false))
                        {
                            using (var derivationFile = new StreamWriter("derivations.txt", false))
                            {
                                using (var tableFile = new StreamWriter("tables.txt", false))
                                {
                                    foreach (var tok in tokens[0])
                                    {
                                        validFile.WriteLine(tok.ToString());
                                    }
                                    if (tokens[1].Count > 0)
                                    {
                                        Console.WriteLine("Lexical errors...");
                                        foreach (var tok in tokens[1])
                                        {
                                            Console.WriteLine(tok.ToString());
                                            invalidFile.WriteLine(tok.ToString());
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Lexical analysis successful.");
                                    }

                                    if (parseSuccesful)
                                    {
                                        Console.WriteLine("Parse successful");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Parse unsuccessful!!");
                                        derivationFile.WriteLine("There were some errors...");

                                        sa.Errors.ForEach((e) => Console.WriteLine(e));
                                        sa.InternalSemanticAnalyse.DumpErrors();
                                    }
                                    derivationFile.WriteLine(String.Format("{0,-50}{1}", "", sa.ParseDerivations.ToString(0)));
                                    for (int i = 0; i < sa.ParseDerivations.AppliedProductions.Count; i++)
                                    {
                                        derivationFile.WriteLine(String.Format("{0,-50}{1}", sa.ParseDerivations.AppliedProductions[i].ToString(), sa.ParseDerivations.ToString(i + 1)));
                                    }
                                    tableFile.Write(sa.InternalSemanticAnalyse.GetTablesOutput());

                                    Console.WriteLine("Warnings:");
                                    foreach (var tok in tokens[1])
                                    {
                                        if (!String.IsNullOrEmpty(tok.Warning))
                                        {
                                            Console.WriteLine(String.Format("Warning ({0}, {1}) : {2}", tok.Position.LineNumber, tok.Position.PositionInLine, tok.Warning));
                                        }
                                    }
                                }   
                            }
                        }
                    }
                    Console.WriteLine("Done");

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Can't load the data in the file.");
                }
            }

            Console.ReadKey();
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Compiler;

namespace CompilerTests
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void ValidIntegers()
        {
            FileIO fio = new FileIO();
            fio.LoadStrings(new List<string> { "0", "100", "   0    ", "30000000000000040", "1 0 1", "0a1" });

            Lexer lex = new Lexer(fio);

            var tokens = lex.ExtractTokens();

            //Check that there should not be any invalid 
            Assert.AreEqual(tokens[0].Count, 9);
            Assert.AreEqual(tokens[1].Count, 0);

            var validTokens = tokens[0];
            Assert.AreEqual(validTokens[0].Type, TokenType.T_NUM_INTEGER);
            Assert.AreEqual(validTokens[0].Value, "0");

            Assert.AreEqual(validTokens[1].Type, TokenType.T_NUM_INTEGER);
            Assert.AreEqual(validTokens[1].Value, "100");

            Assert.AreEqual(validTokens[2].Type, TokenType.T_NUM_INTEGER);
            Assert.AreEqual(validTokens[2].Value, "0");
            Assert.AreEqual(validTokens[2].Position.PositionInLine, 3);

            Assert.AreEqual(validTokens[3].Type, TokenType.T_NUM_INTEGER);
            Assert.AreEqual(validTokens[3].Value, "30000000000000040");

            Assert.AreEqual(validTokens[4].Type, TokenType.T_NUM_INTEGER);
            Assert.AreEqual(validTokens[4].Value, "1");
            Assert.AreEqual(validTokens[4].Position.PositionInLine, 0);

            Assert.AreEqual(validTokens[5].Type, TokenType.T_NUM_INTEGER);
            Assert.AreEqual(validTokens[5].Value, "0");
            Assert.AreEqual(validTokens[5].Position.PositionInLine, 2);

            Assert.AreEqual(validTokens[6].Type, TokenType.T_NUM_INTEGER);
            Assert.AreEqual(validTokens[6].Value, "1");
            Assert.AreEqual(validTokens[6].Position.PositionInLine, 4);

            Assert.AreEqual(validTokens[7].Type, TokenType.T_NUM_INTEGER);
            Assert.AreEqual(validTokens[7].Value, "0");

            Assert.AreEqual(validTokens[8].Type, TokenType.T_ID);
            Assert.AreEqual(validTokens[8].Value, "a1");
            Assert.AreEqual(validTokens[8].Position.PositionInLine, 1);
        }

        [TestMethod]
        public void InvalidIntegers()
        {
            FileIO fio = new FileIO();
            fio.LoadStrings(new List<string> { "00", "0100", "   000    ", "030000000000000040", "1<00==1a"});

            Lexer lex = new Lexer(fio);

            var tokens = lex.ExtractTokens();

            //Check that there should not be any invalid 
            Assert.AreEqual(tokens[0].Count, 5);
            Assert.AreEqual(tokens[1].Count, 5);

            var invalidTokens = tokens[1];
            Assert.AreEqual(invalidTokens[0].Type, TokenType.T_ERR_LEADING_ZERO);
            Assert.AreEqual(invalidTokens[0].Value, "00");
                            
            Assert.AreEqual(invalidTokens[1].Type, TokenType.T_ERR_LEADING_ZERO);
            Assert.AreEqual(invalidTokens[1].Value, "0100");
                            
            Assert.AreEqual(invalidTokens[2].Type, TokenType.T_ERR_LEADING_ZERO);
            Assert.AreEqual(invalidTokens[2].Value, "000");
            Assert.AreEqual(invalidTokens[2].Position.PositionInLine, 3);
                            
            Assert.AreEqual(invalidTokens[3].Type, TokenType.T_ERR_LEADING_ZERO);
            Assert.AreEqual(invalidTokens[3].Value, "030000000000000040");
                            
            Assert.AreEqual(invalidTokens[4].Type, TokenType.T_ERR_LEADING_ZERO);
            Assert.AreEqual(invalidTokens[4].Value, "00");
            Assert.AreEqual(invalidTokens[4].Position.PositionInLine, 2);


            var validTokens = tokens[0];

            Assert.AreEqual(validTokens[0].Type, TokenType.T_NUM_INTEGER);
            Assert.AreEqual(validTokens[0].Value, "1");
            Assert.AreEqual(validTokens[0].Position.PositionInLine, 0);

            Assert.AreEqual(validTokens[1].Type, TokenType.T_LESS);
            Assert.AreEqual(validTokens[1].Value, "<");
            Assert.AreEqual(validTokens[1].Position.PositionInLine, 1);
            
            Assert.AreEqual(validTokens[2].Type, TokenType.T_EQUALS);
            Assert.AreEqual(validTokens[2].Value, "==");
            Assert.AreEqual(validTokens[2].Position.PositionInLine, 4);

            Assert.AreEqual(validTokens[3].Type, TokenType.T_NUM_INTEGER);
            Assert.AreEqual(validTokens[3].Value, "1");
            Assert.AreEqual(validTokens[3].Position.PositionInLine, 6);

            Assert.AreEqual(validTokens[4].Type, TokenType.T_ID);
            Assert.AreEqual(validTokens[4].Value, "a");
            Assert.AreEqual(validTokens[4].Position.PositionInLine, 7);
        }

        [TestMethod]
        public void ValidFloat()
        {
            FileIO fio = new FileIO();
            fio.LoadStrings(new List<string> { "0.0", "100.0", "   0.33555    ", "30000000000000040.23150000000005", "1.0 230.0 11.5", "0.5a1.5" });

            Lexer lex = new Lexer(fio);

            var tokens = lex.ExtractTokens();

            //Check that there should not be any invalid 
            Assert.AreEqual(tokens[0].Count, 11);
            Assert.AreEqual(tokens[1].Count, 0);

            var validTokens = tokens[0];
            Assert.AreEqual(validTokens[0].Type, TokenType.T_NUM_FLOAT);
            Assert.AreEqual(validTokens[0].Value, "0.0");

            Assert.AreEqual(validTokens[1].Type, TokenType.T_NUM_FLOAT);
            Assert.AreEqual(validTokens[1].Value, "100.0");

            Assert.AreEqual(validTokens[2].Type, TokenType.T_NUM_FLOAT);
            Assert.AreEqual(validTokens[2].Value, "0.33555");
            Assert.AreEqual(validTokens[2].Position.PositionInLine, 3);

            Assert.AreEqual(validTokens[3].Type, TokenType.T_NUM_FLOAT);
            Assert.AreEqual(validTokens[3].Value, "30000000000000040.23150000000005");

            Assert.AreEqual(validTokens[4].Type, TokenType.T_NUM_FLOAT);
            Assert.AreEqual(validTokens[4].Value, "1.0");
            Assert.AreEqual(validTokens[4].Position.PositionInLine, 0);

            Assert.AreEqual(validTokens[5].Type, TokenType.T_NUM_FLOAT);
            Assert.AreEqual(validTokens[5].Value, "230.0");
            Assert.AreEqual(validTokens[5].Position.PositionInLine, 4);

            Assert.AreEqual(validTokens[6].Type, TokenType.T_NUM_FLOAT);
            Assert.AreEqual(validTokens[6].Value, "11.5");
            Assert.AreEqual(validTokens[6].Position.PositionInLine, 10);

            Assert.AreEqual(validTokens[7].Type, TokenType.T_NUM_FLOAT);
            Assert.AreEqual(validTokens[7].Value, "0.5");

            Assert.AreEqual(validTokens[8].Type, TokenType.T_ID);
            Assert.AreEqual(validTokens[8].Value, "a1");
            Assert.AreEqual(validTokens[8].Position.PositionInLine, 3);

            Assert.AreEqual(validTokens[9].Type, TokenType.T_DOT);
            Assert.AreEqual(validTokens[9].Value, ".");
            Assert.AreEqual(validTokens[9].Position.PositionInLine, 5);

            Assert.AreEqual(validTokens[10].Type, TokenType.T_NUM_INTEGER);
            Assert.AreEqual(validTokens[10].Value, "5");
            Assert.AreEqual(validTokens[10].Position.PositionInLine, 6);
        }

        [TestMethod]
        public void InvalidIFloat()
        {
            FileIO fio = new FileIO();
            fio.LoadStrings(new List<string> { "00.0", "100.00", "   00.335550    ", "30000000000000040.231500000000050", "01.0 1.00 01.00 1." });

            Lexer lex = new Lexer(fio);

            var tokens = lex.ExtractTokens();

            //Check that there should not be any invalid 
            Assert.AreEqual(tokens[0].Count, 0);
            Assert.AreEqual(tokens[1].Count, 8);

            //If leading AND trailing zeros are present, LEADING_ZERO has precedence.
            var invalidTokens = tokens[1];
            Assert.AreEqual(invalidTokens[0].Type, TokenType.T_ERR_LEADING_ZERO);
            Assert.AreEqual(invalidTokens[0].Value, "00.0");

            Assert.AreEqual(invalidTokens[1].Type, TokenType.T_ERR_TRAILING_ZERO);
            Assert.AreEqual(invalidTokens[1].Value, "100.00");

            Assert.AreEqual(invalidTokens[2].Type, TokenType.T_ERR_LEADING_ZERO);
            Assert.AreEqual(invalidTokens[2].Value, "00.335550");
            Assert.AreEqual(invalidTokens[2].Position.PositionInLine, 3);

            Assert.AreEqual(invalidTokens[3].Type, TokenType.T_ERR_TRAILING_ZERO);
            Assert.AreEqual(invalidTokens[3].Value, "30000000000000040.231500000000050");

            Assert.AreEqual(invalidTokens[4].Type, TokenType.T_ERR_LEADING_ZERO);
            Assert.AreEqual(invalidTokens[4].Value, "01.0");
            Assert.AreEqual(invalidTokens[4].Position.PositionInLine, 0);

            Assert.AreEqual(invalidTokens[5].Type, TokenType.T_ERR_TRAILING_ZERO);
            Assert.AreEqual(invalidTokens[5].Value, "1.00");
            Assert.AreEqual(invalidTokens[5].Position.PositionInLine, 5);

            Assert.AreEqual(invalidTokens[6].Type, TokenType.T_ERR_LEADING_ZERO);
            Assert.AreEqual(invalidTokens[6].Value, "01.00");
            Assert.AreEqual(invalidTokens[6].Position.PositionInLine, 10);

            Assert.AreEqual(invalidTokens[7].Type, TokenType.T_ERR_INVALID_FLOAT);
            Assert.AreEqual(invalidTokens[7].Value, "1.");
            Assert.AreEqual(invalidTokens[7].Position.PositionInLine, 16);
        }

        [TestMethod]
        public void OperatorTests()
        {
            FileIO fio = new FileIO();
            fio.LoadStrings(new List<string> { "+-*/<><====>=><", "1<1 1   +    0//6", });

            Lexer lex = new Lexer(fio);

            var tokens = lex.ExtractTokens();

            //Check that there should not be any invalid 
            Assert.AreEqual(tokens[0].Count, 18);
            Assert.AreEqual(tokens[1].Count, 0);

            var validTokens = tokens[0];

            Assert.AreEqual(validTokens[0].Type, TokenType.T_PLUS);
            Assert.AreEqual(validTokens[0].Value, "+");
            Assert.AreEqual(validTokens[0].Position.PositionInLine, 0);

            Assert.AreEqual(validTokens[1].Type, TokenType.T_MINUS);
            Assert.AreEqual(validTokens[1].Value, "-");
            Assert.AreEqual(validTokens[1].Position.PositionInLine, 1);

            Assert.AreEqual(validTokens[2].Type, TokenType.T_MULTIPLICATION);
            Assert.AreEqual(validTokens[2].Value, "*");
            Assert.AreEqual(validTokens[2].Position.PositionInLine, 2);

            Assert.AreEqual(validTokens[3].Type, TokenType.T_DIVISION);
            Assert.AreEqual(validTokens[3].Value, "/");
            Assert.AreEqual(validTokens[3].Position.PositionInLine, 3);

            Assert.AreEqual(validTokens[4].Type, TokenType.T_N_EQUALS);
            Assert.AreEqual(validTokens[4].Value, "<>");
            Assert.AreEqual(validTokens[4].Position.PositionInLine, 4);

            Assert.AreEqual(validTokens[5].Type, TokenType.T_LESS_EQUAL);
            Assert.AreEqual(validTokens[5].Value, "<=");
            Assert.AreEqual(validTokens[5].Position.PositionInLine, 6);

            Assert.AreEqual(validTokens[6].Type, TokenType.T_EQUALS);
            Assert.AreEqual(validTokens[6].Value, "==");
            Assert.AreEqual(validTokens[6].Position.PositionInLine, 8);

            Assert.AreEqual(validTokens[7].Type, TokenType.T_ASSIGNMENT);
            Assert.AreEqual(validTokens[7].Value, "=");
            Assert.AreEqual(validTokens[7].Position.PositionInLine, 10);

            Assert.AreEqual(validTokens[8].Type, TokenType.T_GREATER_EQUAL);
            Assert.AreEqual(validTokens[8].Value, ">=");
            Assert.AreEqual(validTokens[8].Position.PositionInLine, 11);

            Assert.AreEqual(validTokens[9].Type, TokenType.T_GREATER);
            Assert.AreEqual(validTokens[9].Value, ">");
            Assert.AreEqual(validTokens[9].Position.PositionInLine, 13);

            Assert.AreEqual(validTokens[10].Type, TokenType.T_LESS);
            Assert.AreEqual(validTokens[10].Value, "<");
            Assert.AreEqual(validTokens[10].Position.PositionInLine, 14);

            Assert.AreEqual(validTokens[12].Type, TokenType.T_LESS);
            Assert.AreEqual(validTokens[12].Value, "<");
            Assert.AreEqual(validTokens[12].Position.PositionInLine, 1);

            Assert.AreEqual(validTokens[15].Type, TokenType.T_PLUS);
            Assert.AreEqual(validTokens[15].Value, "+");
            Assert.AreEqual(validTokens[15].Position.PositionInLine, 8);

            Assert.AreEqual(validTokens[17].Type, TokenType.T_COMMENT);
            Assert.AreEqual(validTokens[17].Value, "//6");
            Assert.AreEqual(validTokens[17].Position.PositionInLine, 14);

        }

        [TestMethod]
        public void PuncTests()
        {
            FileIO fio = new FileIO();
            fio.LoadStrings(new List<string> { ".,;][{}()", "(1)<.1 a1;", });

            Lexer lex = new Lexer(fio);

            var tokens = lex.ExtractTokens();

            //Check that there should not be any invalid 
            Assert.AreEqual(tokens[0].Count, 17);
            Assert.AreEqual(tokens[1].Count, 0);

            var validTokens = tokens[0];

            Assert.AreEqual(validTokens[0].Type, TokenType.T_DOT);
            Assert.AreEqual(validTokens[0].Value, ".");
            Assert.AreEqual(validTokens[0].Position.PositionInLine, 0);

            Assert.AreEqual(validTokens[1].Type, TokenType.T_COMMA);
            Assert.AreEqual(validTokens[1].Value, ",");
            Assert.AreEqual(validTokens[1].Position.PositionInLine, 1);

            Assert.AreEqual(validTokens[2].Type, TokenType.T_SEMI_COLUMN);
            Assert.AreEqual(validTokens[2].Value, ";");
            Assert.AreEqual(validTokens[2].Position.PositionInLine, 2);

            Assert.AreEqual(validTokens[3].Type, TokenType.T_CLOSED_S_BRACKET);
            Assert.AreEqual(validTokens[3].Value, "]");
            Assert.AreEqual(validTokens[3].Position.PositionInLine, 3);

            Assert.AreEqual(validTokens[4].Type, TokenType.T_OPENED_S_BRACKET);
            Assert.AreEqual(validTokens[4].Value, "[");
            Assert.AreEqual(validTokens[4].Position.PositionInLine, 4);

            Assert.AreEqual(validTokens[5].Type, TokenType.T_OPENED_BRACKET);
            Assert.AreEqual(validTokens[5].Value, "{");
            Assert.AreEqual(validTokens[5].Position.PositionInLine, 5);

            Assert.AreEqual(validTokens[6].Type, TokenType.T_CLOSED_BRACKET);
            Assert.AreEqual(validTokens[6].Value, "}");
            Assert.AreEqual(validTokens[6].Position.PositionInLine, 6);

            Assert.AreEqual(validTokens[7].Type, TokenType.T_OPENED_PAR);
            Assert.AreEqual(validTokens[7].Value, "(");
            Assert.AreEqual(validTokens[7].Position.PositionInLine, 7);

            Assert.AreEqual(validTokens[8].Type, TokenType.T_CLOSED_PAR);
            Assert.AreEqual(validTokens[8].Value, ")");
            Assert.AreEqual(validTokens[8].Position.PositionInLine, 8);

            Assert.AreEqual(validTokens[9].Type, TokenType.T_OPENED_PAR);
            Assert.AreEqual(validTokens[9].Value, "(");
            Assert.AreEqual(validTokens[9].Position.PositionInLine, 0);

            Assert.AreEqual(validTokens[11].Type, TokenType.T_CLOSED_PAR);
            Assert.AreEqual(validTokens[11].Value, ")");
            Assert.AreEqual(validTokens[11].Position.PositionInLine, 2);

            Assert.AreEqual(validTokens[13].Type, TokenType.T_DOT);
            Assert.AreEqual(validTokens[13].Value, ".");
            Assert.AreEqual(validTokens[13].Position.PositionInLine, 4);

            Assert.AreEqual(validTokens[16].Type, TokenType.T_SEMI_COLUMN);
            Assert.AreEqual(validTokens[16].Value, ";");
            Assert.AreEqual(validTokens[16].Position.PositionInLine, 9);


        }

        [TestMethod]
        public void IdTests()
        {
            FileIO fio = new FileIO();
            fio.LoadStrings(new List<string> { "and,if,then,else,or,return,get,put,class, int,float,not", "anda And a1_56 1a _o_9 (a1)", });

            Lexer lex = new Lexer(fio);

            var tokens = lex.ExtractTokens();
                      

            //Check that there should not be any invalid 
            Assert.AreEqual(tokens[0].Count, 32);
            Assert.AreEqual(tokens[1].Count, 1);

            var validTokens = tokens[0];

            Assert.AreEqual(validTokens[23].Type, TokenType.T_ID);
            Assert.AreEqual(validTokens[23].Value, "anda");

            Assert.AreEqual(validTokens[24].Type, TokenType.T_ID);
            Assert.AreEqual(validTokens[24].Value, "And");

            Assert.AreEqual(validTokens[25].Type, TokenType.T_ID);
            Assert.AreEqual(validTokens[25].Value, "a1_56");

            Assert.AreEqual(validTokens[26].Type, TokenType.T_NUM_INTEGER);
            Assert.AreEqual(validTokens[26].Value, "1");

            Assert.AreEqual(validTokens[27].Type, TokenType.T_ID);
            Assert.AreEqual(validTokens[27].Value, "a");

            Assert.AreEqual(validTokens[28].Type, TokenType.T_ID);
            Assert.AreEqual(validTokens[28].Value, "o_9");

            Assert.AreEqual(validTokens[29].Type, TokenType.T_OPENED_PAR);
            Assert.AreEqual(validTokens[29].Value, "(");

            Assert.AreEqual(validTokens[30].Type, TokenType.T_ID);
            Assert.AreEqual(validTokens[30].Value, "a1");

            Assert.AreEqual(validTokens[31].Type, TokenType.T_CLOSED_PAR);
            Assert.AreEqual(validTokens[31].Value, ")");

            Assert.AreEqual(validTokens[2].Type, TokenType.T_IF);
            Assert.AreEqual(validTokens[2].Value, "if");

            Assert.AreEqual(validTokens[4].Type, TokenType.T_THEN);
            Assert.AreEqual(validTokens[4].Value, "then");

            Assert.AreEqual(validTokens[6].Type, TokenType.T_ELSE);
            Assert.AreEqual(validTokens[6].Value, "else");
            

            Assert.AreEqual(validTokens[8].Type, TokenType.T_OR);
            Assert.AreEqual(validTokens[8].Value, "or");
            

            Assert.AreEqual(validTokens[10].Type, TokenType.T_RETURN);
            Assert.AreEqual(validTokens[10].Value, "return");
            

            Assert.AreEqual(validTokens[12].Type, TokenType.T_GET);
            Assert.AreEqual(validTokens[12].Value, "get");

            Assert.AreEqual(validTokens[14].Type, TokenType.T_PUT);
            Assert.AreEqual(validTokens[14].Value, "put");

            Assert.AreEqual(validTokens[16].Type, TokenType.T_CLASS);
            Assert.AreEqual(validTokens[16].Value, "class");

            Assert.AreEqual(validTokens[18].Type, TokenType.T_INT);
            Assert.AreEqual(validTokens[18].Value, "int");

            Assert.AreEqual(validTokens[20].Type, TokenType.T_FLOAT);
            Assert.AreEqual(validTokens[20].Value, "float");

            Assert.AreEqual(validTokens[22].Type, TokenType.T_NOT);
            Assert.AreEqual(validTokens[22].Value, "not");

            //Assert.AreEqual(validTokens[16].Type, TokenType.T_SEMI_COLUMN);
            //Assert.AreEqual(validTokens[16].Value, ";");


        }

        [TestMethod]
        public void CommentsTests()
        {


            FileIO fio = new FileIO();
            fio.LoadStrings(new List<string> { "/* skjb fksdb and ()4u3ihoyfboa **", " ////////\nd gndfk l fhf gd */",
                "and or //this is a comment lol and or 12234 __ %%%", "/* djfk nds", "sdfsdf/* */*/", "/* kjsdbf kjhbfds kjhbds f" });

            Lexer lex = new Lexer(fio);

            var tokens = lex.ExtractTokens();


            //Check that there should not be any invalid 
            Assert.AreEqual(tokens[0].Count, 7);
            Assert.AreEqual(tokens[1].Count, 1);

            var invalidTokens = tokens[1];
            Assert.AreEqual(invalidTokens[0].Type, TokenType.T_ERR_INVALID_COMMENT_BLOCK);
            Assert.AreEqual(invalidTokens[0].Value, "/* kjsdbf kjhbfds kjhbds f");

            var validTokens = tokens[0];

            Assert.AreEqual(validTokens[0].Type, TokenType.T_COMMENT);
            Assert.AreEqual(validTokens[0].Value, "/* skjb fksdb and ()4u3ihoyfboa ** ////////\nd gndfk l fhf gd */");

            Assert.AreEqual(validTokens[1].Type, TokenType.T_AND);
            Assert.AreEqual(validTokens[1].Value, "and");

            Assert.AreEqual(validTokens[2].Type, TokenType.T_OR);
            Assert.AreEqual(validTokens[2].Value, "or");

            Assert.AreEqual(validTokens[3].Type, TokenType.T_COMMENT);
            Assert.AreEqual(validTokens[3].Value, "//this is a comment lol and or 12234 __ %%%");

            Assert.AreEqual(validTokens[4].Type, TokenType.T_COMMENT);
            Assert.AreEqual(validTokens[4].Value, "/* djfk ndssdfsdf/* */");

            Assert.AreEqual(validTokens[5].Type, TokenType.T_MULTIPLICATION);
            Assert.AreEqual(validTokens[5].Value, "*");

            Assert.AreEqual(validTokens[6].Type, TokenType.T_DIVISION);
            Assert.AreEqual(validTokens[6].Value, "/");

            
        }

        //[TestMethod]
        //public void TestMethod1()
        //{
        //    FileIO fio = new FileIO();
        //    fio.LoadStrings(new List<string> { "sadsa dsa" });

        //    Lexer lex = new Lexer(fio);


        //}

        //[TestMethod]
        //public void TestMethod1()
        //{
        //    FileIO fio = new FileIO();
        //    fio.LoadStrings(new List<string> { "sadsa dsa" });

        //    Lexer lex = new Lexer(fio);


        //}

        //[TestMethod]
        //public void TestMethod1()
        //{
        //    FileIO fio = new FileIO();
        //    fio.LoadStrings(new List<string> { "sadsa dsa" });

        //    Lexer lex = new Lexer(fio);


        //}

        //[TestMethod]
        //public void TestMethod1()
        //{
        //    FileIO fio = new FileIO();
        //    fio.LoadStrings(new List<string> { "sadsa dsa" });

        //    Lexer lex = new Lexer(fio);


        //}

        //[TestMethod]
        //public void TestMethod1()
        //{
        //    FileIO fio = new FileIO();
        //    fio.LoadStrings(new List<string> { "sadsa dsa" });

        //    Lexer lex = new Lexer(fio);


        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser
{
    public class DimensionnedElement
    {
        private List<int> _dimensions;
        private Token _associated_token;
        public List<int> Dimensions
        {
            get
            {
                return _dimensions;
            }
        }
        public Token AssociatedToken
        {
            get
            {
                return _associated_token;
            }

        }
        public DimensionnedElement (List<int> dimensions, Token assToken)
        {
            _dimensions = dimensions;
            _associated_token = assToken;
        }
    }
}

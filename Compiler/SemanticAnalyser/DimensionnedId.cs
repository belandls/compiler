﻿using Compiler.SemanticAnalyser.SemanticActions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser
{
    public class DimensionnedId
    {
        private string _id_name;
        private List<Expression> _dimensions;
        private Token _associated_token;
        private List<int> _reference_dimension;
        public List<Expression> Dimensions
        {
            get
            {
                return _dimensions;
            }
        }
        public Token AssociatedToken
        {
            get
            {
                return _associated_token;
            }

        }

        public DimensionnedId(string idName, Token assToken) : this(idName, new List<Expression>(), assToken) { }
        public DimensionnedId(string idName, List<Expression> dimensions, Token assToken)
        {
            _id_name = idName;
            _dimensions = dimensions;
            _associated_token = assToken;
        }

        public string IdName
        {
            get
            {
                return _id_name;
            }
        }

        public List<int> ReferenceDimensions
        {
            get
            {
                return _reference_dimension;
            }

            set
            {
                _reference_dimension = value;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(_id_name);
            foreach(var dim in _dimensions)
            {
                sb.Append('[').Append(dim.ToString()).Append(']');
            }
            return sb.ToString();
        }
    }
}

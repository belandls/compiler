﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class ExpressionFunctionGatherAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction temp = sa.SemanticStack.Pop();
            ParameterizedNestedId pnid = (ParameterizedNestedId)temp.SemanticValue;
            Expression.ExpressionStackElement ese = null;

            bool valid = true;
            SymbolEntry se = null;
            DimensionnedId currentNid = pnid.Nid.Initial;
            SymbolTable tempTable = sa.CurrentTable;
            bool stillIterating;
            var idEnum = pnid.Nid.Ids.GetEnumerator();
            stillIterating = idEnum.MoveNext();
            while (valid)
            {
                var res = tempTable.LookupByNameAndType(currentNid.IdName, SymbolEntry.KindOfSymbol.VARIABLE | SymbolEntry.KindOfSymbol.PARAMETER);
                if (res != null)
                {
                    if (currentNid.Dimensions.Count == res.Types[0].Dimensions.Count)
                    {
                        if (stillIterating)
                        {
                            var classEntry = tempTable.LookupByNameAndType(res.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS);
                            if (classEntry == null)
                            {
                                valid = false;
                                break;
                            }
                            else
                            {
                                currentNid = idEnum.Current;
                                tempTable = classEntry.LinkedTable;
                                stillIterating = idEnum.MoveNext();
                            }
                        }
                        else
                        {
                            se = res;
                            break;
                        }
                    }
                    else
                    {
                        if(sa.CurrentPass == Pass.FIRST)
                        {
                            if (currentNid.Dimensions.Count > res.Types[0].Dimensions.Count)
                            {
                                sa.Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_ARRAY_ACCESS, currentNid.AssociatedToken));
                            }
                            else
                            {
                                sa.Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_ARRAY_OP, currentNid.AssociatedToken));
                            }
                        }
                        valid = false;
                        break;
                    }
                }
                else
                {
                    if (!stillIterating)
                    {
                        if(pnid.Nid.Ids.Count > 0)
                        {
                            //nested, should only look in local table
                            res = tempTable.LookupByNameAndType(currentNid.IdName, SymbolEntry.KindOfSymbol.FUNCTION, true);
                        }
                        else
                        {
                            //simple function call
                            res = tempTable.LookupByNameAndType(currentNid.IdName, SymbolEntry.KindOfSymbol.FUNCTION, false);
                        }
                        
                        if(res == null)
                        {
                            valid = false;
                            if(sa.CurrentPass == Pass.SECOND)
                            {
                                sa.Errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_FN, currentNid.AssociatedToken));
                            }
                        }
                        else
                        {
                            if(res.Types.Count - 1 == pnid.Params.Count)
                            {
                                for(int i = 0; i < pnid.Params.Count; ++i)
                                {
                                    if(res.Types[i+1].Type == Types.INT && pnid.Params[0].ValType != Expression.ExpressionValueType.INTEGER ||
                                        res.Types[i + 1].Type == Types.FLOAT && pnid.Params[0].ValType != Expression.ExpressionValueType.FLOAT||
                                        res.Types[i + 1].Type == Types.CLASS && pnid.Params[0].ValType != Expression.ExpressionValueType.CLASS ||
                                        res.Types[i + 1].Type == Types.CLASS && pnid.Params[0].ValType == Expression.ExpressionValueType.CLASS && res.Types[i + 1].ClassName != pnid.Params[0].ClassName)
                                    {
                                        valid = false;
                                        if (sa.CurrentPass == Pass.SECOND)
                                            sa.Errors.Add(new SemanticError(SemanticError.ErrorType.UNEXPECTED_TYPE_FN_PARAM, nextToken, new Tuple<int,string, string>(i + 1, res.Types[i + 1].Type.ToString(),pnid.Params[i].ValType.ToString())));
                                    }else //if (res.Types[i+1].Dimensions.Count != pnid.Nid.Ids.ElementAt(i).Dimensions.Count)
                                    {
                                        if(res.Types[i + 1].Dimensions.Count > 0)
                                        {

                                        }
                                        int i2 = 0;
                                    }
                                }
                                if (valid)
                                {
                                    se = res;
                                }
                                break;
                            }
                            else
                            {
                                if(sa.CurrentPass == Pass.SECOND)
                                {
                                    sa.Errors.Add(new SemanticError(SemanticError.ErrorType.UNEXPECTED_PARAMETER_COUNT, pnid.Nid.Initial.AssociatedToken, pnid.Params.Count));
                                }
                                
                                valid = false;
                            }
                        }
                    }
                    else {
                        if (sa.CurrentPass == Pass.FIRST)
                        {
                            sa.Errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_VAR, currentNid.AssociatedToken));
                        }
                            valid = false;
                        break;
                    }
                }
            }
            if (valid)
            {
                if (se.Types[0].Type == Types.INT)
                {
                    ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.RETURN_INT, Expression.ExpressionStackElement.Cardinality.NA, pnid, nextToken);
                }
                else if (se.Types[0].Type == Types.FLOAT)
                {
                    ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.RETURN_FLOAT, Expression.ExpressionStackElement.Cardinality.NA, pnid, nextToken);
                }
                else
                {
                    ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.RETURN_CLASS, Expression.ExpressionStackElement.Cardinality.NA, pnid, nextToken);
                }
            }
            else
            {
                ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.EMPTY, Expression.ExpressionStackElement.Cardinality.NA, new object(), nextToken);
            }

            SemanticValue = ese;
        }
    }
}

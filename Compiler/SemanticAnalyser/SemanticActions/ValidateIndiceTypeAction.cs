﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class ValidateIndiceTypeAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction temp = sa.SemanticStack.Pop();
            Expression exp = (Expression)temp.SemanticValue;

            if(sa.CurrentPass == Pass.SECOND && exp.ValType != Expression.ExpressionValueType.INTEGER)
            {
                sa.Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_INDICE_TYPE, nextToken));
            }
            SemanticValue = exp;
        }
    }
}

﻿using Compiler.SyntaxAnalyserComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    public class SemanticActionFactory : Symbol
    {
        private string _semantic_action_num;

        public SemanticActionFactory(string value) : base(value,false)
        {
            _semantic_action_num = value.Substring(1, value.Length - 2);
            
        }

        public SemanticAction GeneratedAction
        {
            get
            {
                switch (_semantic_action_num)
                {
                    case "1":
                        return new TypeGatherAction();
                        break;
                    case "2":
                        return new IdNameGatherAction();
                        break;
                    case "3":
                        return new TypedIdAggregationAction();
                        break;
                    case "4":
                        return new CreateVarEntryAction();
                        break;
                    case "5":
                        return new DimensionAggregationAction();
                        break;
                    case "6":
                        return new GatherDimensionAction();
                        break;
                    case "8":
                        return new ChangeToIdNameAction();
                        break;
                    case "9":
                        return new CreateFnTableAction();
                        break;
                    case "10":
                        return new FnParamAggregationAction();
                        break;
                    case "11":
                        return new CreateProgramTableAction();
                        break;
                    case "12":
                        return new CreateRootTableAction();
                        break;
                    case "13":
                        return new CreateClassTableAction();
                        break;
                    case "14":
                        return new CreateForTableAction();
                        break;
                    case "15":
                        return new NestedIdGatherAction();
                        break;
                    case "16":
                        return new NestedIdAggregationAction();
                        break;
                    case "17":
                        return new CreateVarAccessAction();
                        break;
                    case "18":
                        return new ParameterizedTypedIdAggregationAction();
                        break;
                    case "19":
                        return new CreateFunctionAccessAction();
                        break;
                    case "20":
                        return new StartExpressionAction();
                        break;
                    case "21":
                        return new ExpressionElementGatherAction();
                        break;
                    case "22":
                        return new AggregateExpressionAction();
                        break;
                    case "23":
                        return new EndExpressionAction();
                        break;
                    case "24":
                        return new DimensionnedIdGatherAction();
                        break;
                    case "25":
                        return new DimensionnedIdAggregationAction();
                        break;
                    case "26":
                        return new ExpressionVariableGatherAction();
                        break;
                    case "27":
                        return new ParameterizedNidGatherAction();
                        break;
                    case "28":
                        return new ParameterizedNidAggregationAction();
                        break;
                    case "29":
                        return new ExpressionFunctionGatherAction();
                        break;
                    case "30":
                        return new GenerateReturnStatementAction();
                        break;
                    case "31":
                        return new ValidateIndiceTypeAction();
                        break;
                    case "50":
                        return new GenerateStackReservationAction();
                    case "51":
                        return new GenerateExpressionAction();
                        break;
                    case "52":
                        return new GenerateBegIfAction();
                        break;
                    case "53":
                        return new GenerateElseAction();
                        break;
                    case "54":
                        return new GenerateEndIf();
                    case "55":
                        return new GenerateForHeaderAction();
                        break;
                    case "56":
                        return new GenerateForFooterAction();
                        break;
                    case "57":
                        return new GeneratePutAction();
                        break;
                    case "58":
                        return new GenerateGetAction();
                        break;
                    case "59":
                        return new GenerateFnCallAction();
                        break;
                    case "60":
                        return new GenerateBegFnDefAction();
                        break;
                    case "61":
                        return new GenerateEndFnDefAction();
                        break;
                    case "62":
                        return new GenerateEndProgramAction();
                        break;
                    case "99":
                        return new ScopeEndAction();
                        break;
                }
                return null;
            }
        }
    }
}

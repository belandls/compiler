﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class ParameterizedTypedIdAggregationAction : SemanticAction
    {
     

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction tempIdNameAction = sa.SemanticStack.Pop();
            TypeGatherAction tempTypeAction = (TypeGatherAction)sa.SemanticStack.Pop();
            string valName = (string)tempIdNameAction.SemanticValue;
            Tuple<Types, string, Token> valType = (Tuple<Types, string, Token >)tempTypeAction.SemanticValue;
            ParameterizedTypedId ptid = null;
            if (valType.Item2 == null)
            {
                ptid = new ParameterizedTypedId(valType.Item1, valName, nextToken, valType.Item3);
            }
            else
            {
                ptid = new ParameterizedTypedId(valType.Item2, valName, nextToken, valType.Item3);
            }
            if (sa.CurrentPass == Pass.SECOND)
            {
                if (valType.Item2 == null)
                {
                    ptid.Resolved = true;
                }
                else
                {
                    var res = sa.CurrentTable.LookupByNameAndType(ptid.ClassName, SymbolEntry.KindOfSymbol.CLASS);
                    if (res != null)
                    {
                        ptid.Resolved = true;
                    }
                    else
                    {
                        sa.Errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_TYPE, ptid.TypeToken));
                    }
                }

            }
            SemanticValue = ptid;
        }
    }
}

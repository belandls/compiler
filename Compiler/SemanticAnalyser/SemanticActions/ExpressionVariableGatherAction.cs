﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class ExpressionVariableGatherAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction temp = sa.SemanticStack.Pop();
            NestedId nid = (NestedId)temp.SemanticValue;
            Expression.ExpressionStackElement ese = null;

            if (nid.Ids.Count == 0)
            {
                var res = sa.CurrentTable.LookupByNameAndType(nid.Initial.IdName, SymbolEntry.KindOfSymbol.VARIABLE | SymbolEntry.KindOfSymbol.PARAMETER);
                if (res != null)
                {
                    if (nid.Initial.Dimensions.Count == res.Types[0].Dimensions.Count)
                    {
                        nid.Initial.ReferenceDimensions = res.Types[0].Dimensions;
                        if (res.Types[0].Type == Types.INT)
                        {
                            ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.VARIABLE_INT, Expression.ExpressionStackElement.Cardinality.NA, nid, nid.Associated_token);
                        }
                        else if (res.Types[0].Type == Types.FLOAT)
                        {
                            ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.VARIABLE_FLOAT, Expression.ExpressionStackElement.Cardinality.NA, nid, nid.Associated_token);
                        }
                        else
                        {
                            ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.VARIABLE_CLASS, res.Name, Expression.ExpressionStackElement.Cardinality.NA, nid, nid.Associated_token);
                        }
                    }
                    else
                    {
                        if(nid.Initial.Dimensions.Count == 0)
                        {
                            nid.Initial.ReferenceDimensions = res.Types[0].Dimensions;
                            ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.VARIABLE_INT_ARRAY, Expression.ExpressionStackElement.Cardinality.NA, nid, nextToken);
                        }
                        else
                        {
                            ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.EMPTY, Expression.ExpressionStackElement.Cardinality.NA, null, nextToken);
                        }
                        if (sa.CurrentPass == Pass.FIRST)
                        {
                            if (nid.Initial.Dimensions.Count > res.Types[0].Dimensions.Count)
                            {
                                sa.Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_ARRAY_ACCESS, nid.Initial.AssociatedToken));
                            }
                            else
                            {
                                sa.Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_ARRAY_OP, nid.Initial.AssociatedToken));
                            }
                        }

                    }
                }
                else
                {
                    if (sa.CurrentPass == Pass.FIRST)
                        sa.Errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_VAR, nid.Initial.AssociatedToken));
                    ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.EMPTY, Expression.ExpressionStackElement.Cardinality.NA, null, nid.Initial.AssociatedToken);
                }
            }
            else
            {
                bool valid = true;
                Types varType = Types.VOID;
                DimensionnedId currentNid = nid.Initial;
                SymbolTable tempTable = sa.CurrentTable;
                bool stillIterating;
                var idEnum = nid.Ids.GetEnumerator();
                stillIterating = idEnum.MoveNext();
                while (valid)
                {
                    var res = tempTable.LookupByNameAndType(currentNid.IdName, SymbolEntry.KindOfSymbol.VARIABLE | SymbolEntry.KindOfSymbol.PARAMETER);
                    if (res != null)
                    {
                        if (currentNid.Dimensions.Count == res.Types[0].Dimensions.Count)
                        {
                            currentNid.ReferenceDimensions = res.Types[0].Dimensions;
                            if (stillIterating)
                            {
                                var classEntry = tempTable.LookupByNameAndType(res.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS);
                                if (classEntry == null)
                                {
                                    valid = false;
                                    if (sa.CurrentPass == Pass.FIRST)
                                        sa.Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_NESTING, currentNid.AssociatedToken));
                                    break;
                                }
                                else
                                {
                                    currentNid = idEnum.Current;
                                    tempTable = classEntry.LinkedTable;
                                    stillIterating = idEnum.MoveNext();
                                }
                            }
                            else
                            {
                                varType = res.Types[0].Type;
                                break;
                            }
                        }
                        else
                        {
                            valid = false;
                            if (sa.CurrentPass == Pass.FIRST)
                            {
                                if (currentNid.Dimensions.Count > res.Types[0].Dimensions.Count)
                                {
                                    sa.Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_ARRAY_ACCESS, currentNid.AssociatedToken));
                                }
                                else
                                {
                                    sa.Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_ARRAY_OP, currentNid.AssociatedToken));
                                }
                            }
                            break;
                        }
                    }
                    else
                    {
                        valid = false;
                        if (sa.CurrentPass == Pass.FIRST)
                            sa.Errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_VAR, currentNid.AssociatedToken));
                        break;
                    }
                }
                if (valid && varType != Types.VOID)
                {
                    if (varType == Types.INT)
                    {
                        ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.VARIABLE_INT, Expression.ExpressionStackElement.Cardinality.NA, nid, nid.Associated_token);
                    }
                    else if (varType == Types.FLOAT)
                    {
                        ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.VARIABLE_FLOAT, Expression.ExpressionStackElement.Cardinality.NA, nid, nid.Associated_token);
                    }
                    else
                    {
                        ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.VARIABLE_CLASS, tempTable.Name, Expression.ExpressionStackElement.Cardinality.NA, nid, nid.Associated_token);
                    }
                }
                else
                {
                    ese = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.EMPTY, Expression.ExpressionStackElement.Cardinality.NA, null, nextToken);
                }
            }
            SemanticValue = ese;
        }
    }
}

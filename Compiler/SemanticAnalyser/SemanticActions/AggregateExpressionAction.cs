﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class AggregateExpressionAction : SemanticAction
    {

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            Expression.ExpressionStackElement op2 = (Expression.ExpressionStackElement)sa.SemanticStack.Pop().SemanticValue;
            Expression.ExpressionStackElement oper = (Expression.ExpressionStackElement)sa.SemanticStack.Pop().SemanticValue;
            Expression.ExpressionStackElement op1 = null;
            if (oper.Card == Expression.ExpressionStackElement.Cardinality.BINARY)
            {
                op1 = (Expression.ExpressionStackElement)sa.SemanticStack.Pop().SemanticValue;
            }

            //Expression e = (Expression)sa.SemanticStack.Pop().SemanticValue;

            if (sa.CurrentPass == Pass.FIRST && oper.Card == Expression.ExpressionStackElement.Cardinality.UNARY)
            {
                if (op2.Type == Expression.ExpressionStackElement.ElementType.VARIABLE_CLASS)
                {
                    sa.Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_OP_ON_OBJ, op2.AssociatedToken));
                }
            }
            else if (sa.CurrentPass == Pass.FIRST && oper.Card == Expression.ExpressionStackElement.Cardinality.BINARY)
            {
                if ((string)oper.Value != "=")
                {
                    if (op2.Type == Expression.ExpressionStackElement.ElementType.VARIABLE_CLASS || op1.Type == Expression.ExpressionStackElement.ElementType.VARIABLE_CLASS)
                    {
                        sa.Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_OP_ON_OBJ, oper.AssociatedToken));
                    }
                }
                
            }

                Expression e = new Expression();
            if (op1 != null)
            {
                e.AddExpressionElement(op1);
            }
            if (!e.AddExpressionElement(op2))
            {
                if (sa.CurrentPass == Pass.SECOND)
                    sa.Errors.Add(new SemanticError(SemanticError.ErrorType.UNEXPECTED_TYPE_EXP, op2.AssociatedToken, e.ValType));
            }
            e.AddExpressionElement(oper);
            Expression.ExpressionStackElement ese = null;
            if (e.ValType == Expression.ExpressionValueType.INTEGER)
            {
                ese= new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.EXPRESSION_INT, Expression.ExpressionStackElement.Cardinality.NA, e, oper.AssociatedToken);
            }
            else
            {
                ese =new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.EXPRESSION_FLOAT, Expression.ExpressionStackElement.Cardinality.NA, e,oper.AssociatedToken);
            }
            SemanticValue = ese;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class IdNameGatherAction : SemanticAction
    {


        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticValue = nextToken.Value;
            AssociatedToken = nextToken;
        }
    }
}

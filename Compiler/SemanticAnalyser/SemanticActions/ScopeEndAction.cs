﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class ScopeEndAction : SemanticAction
    {
       
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            //Console.WriteLine(sa.CurrentTable.GetTableDump());
            sa.EndCurrentScope();
        }
    }
}

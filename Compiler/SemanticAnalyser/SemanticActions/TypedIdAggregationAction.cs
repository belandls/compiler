﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class TypedIdAggregationAction : SemanticAction
    {

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            IdNameGatherAction tempIdNameAction = (IdNameGatherAction)sa.SemanticStack.Pop();
            SemanticAction tempTypeAction = sa.SemanticStack.Pop();
            string valName = (string)tempIdNameAction.SemanticValue;
            Tuple<Types, string,Token> valType = (Tuple < Types, string, Token> )tempTypeAction.SemanticValue;
            TypedId tempTid = null;
            if (valType.Item2 == null)
            {
                tempTid = new TypedId(valType.Item1, valName, nextToken, valType.Item3);
            }
            else
            {
                tempTid = new TypedId(valType.Item2, valName, nextToken, valType.Item3);
            }

            if(sa.CurrentPass == Pass.SECOND)
            {
                if (valType.Item2 == null)
                {
                    tempTid.Resolved = true;
                }
                else
                {
                    var res = sa.CurrentTable.LookupByNameAndType(tempTid.ClassName, SymbolEntry.KindOfSymbol.CLASS);
                    if (res != null)
                    {
                        tempTid.Resolved = true;
                    }
                    else
                    {
                        sa.Errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_TYPE, tempTid.TypeToken));
                    }
                }
                    
            }
            SemanticValue = tempTid;
        }
    }
}

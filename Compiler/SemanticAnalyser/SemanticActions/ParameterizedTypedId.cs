﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    public class ParameterizedTypedId : TypedId
    {
        private List<TypedId> _parameters;

        public List<TypedId> Parameters
        {
            get
            {
                return _parameters;
            }
        }

        public ParameterizedTypedId(Types type, string idName, Token assToken,  Token typeToken) : this(type, null, idName, new List<int>(), assToken, typeToken) { }
        public ParameterizedTypedId(string className, string idName, Token assToken,  Token typeToken) : this(Types.CLASS, className,idName, new List<int>(), assToken, typeToken) { }
        public ParameterizedTypedId(Types type, string className, string idName, List<int> dimensions, Token assToken,  Token typeToken) : base(type, className, idName, dimensions, assToken,typeToken)
        {
            _parameters = new List<TypedId>();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class CreateClassTableAction : SemanticAction
    {

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            SemanticAction semAct = sa.SemanticStack.Pop();
            string name = (string)semAct.SemanticValue;
            if (sa.CurrentPass == Pass.FIRST)
            {
                sa.CreateTable(name, SymbolTable.TableType.CLASS);
                if (!sa.CurrentTable.Parent.CreateClassEntry(name, sa.CurrentTable))
                {
                    sa.Errors.Add(new SemanticError(SemanticError.ErrorType.MULTIPLY_DEFINED, nextToken));
                }
            }
            else
            {
                sa.SelectTable(name, SymbolTable.TableType.CLASS);
            }
            
        }
    }
}

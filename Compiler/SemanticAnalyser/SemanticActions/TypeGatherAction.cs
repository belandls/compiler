﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class TypeGatherAction : SemanticAction
    {


        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            if (nextToken.Type == TokenType.T_INT)
            {
                SemanticValue = new Tuple<Types, string, Token>(Types.INT, null, nextToken);
            }
            else if (nextToken.Type == TokenType.T_FLOAT)
            {
                SemanticValue = new Tuple<Types, string, Token>(Types.FLOAT, null, nextToken);
            }
            else
            {
                SemanticValue = new Tuple<Types, string, Token>(Types.CLASS, nextToken.Value, nextToken);
            }
        }
    }
}

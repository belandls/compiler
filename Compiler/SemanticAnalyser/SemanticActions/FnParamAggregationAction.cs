﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class FnParamAggregationAction : SemanticAction
    {

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction tidAction = sa.SemanticStack.Pop();
            SemanticAction ptidAction = sa.SemanticStack.Pop();
            ParameterizedTypedId tid = (ParameterizedTypedId)ptidAction.SemanticValue;
            tid.Parameters.Add((TypedId)tidAction.SemanticValue);
            SemanticValue = tid;
            //AssociatedToken = tidAction.AssociatedToken;
        }
    }
}

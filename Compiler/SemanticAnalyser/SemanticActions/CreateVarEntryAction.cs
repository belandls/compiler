﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class CreateVarEntryAction : SemanticAction
    {


        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            SemanticAction temp = sa.SemanticStack.Pop();
            TypedId tid = (TypedId)temp.SemanticValue;
            //sa.CurrentTable.IsRecursiveInstantiation(tid);

            //sa.Errors.Add(new SemanticError(SemanticError.ErrorType.RECURSIVE, tid.AssociatedToken));
            if (sa.CurrentPass == Pass.FIRST || sa.CurrentTable.Type == SymbolTable.TableType.FUNCTION || sa.CurrentTable.Type == SymbolTable.TableType.FOR_SCOPE)
            {
                if (!sa.CurrentTable.CreateVariableEntry(tid, tid.Type != Types.CLASS))
                {
                    if (sa.CurrentPass == Pass.FIRST)
                        sa.Errors.Add(new SemanticError(SemanticError.ErrorType.MULTIPLY_DEFINED, nextToken));
                }
            }
            if (sa.CurrentPass == Pass.SECOND && sa.CurrentTable.Type == SymbolTable.TableType.CLASS)
            {
                sa.CheckForRecursiveMemAlloc(tid);

            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    public class Expression
    {
        public enum ExpressionType
        {
            RELATIONAL,
            ARITHMETIC,
            UNDEFINED
        }

        public enum ExpressionValueType
        {
            INTEGER,
            FLOAT,
            CLASS,
            UNDEFINED
        }

        public class ExpressionStackElement
        {
            public enum ElementType
            {
                IMMEDIATE_INT,
                IMMEDIATE_FLOAT,
                VARIABLE_INT,
                VARIABLE_INT_ARRAY,
                VARIABLE_FLOAT,
                VARIABLE_CLASS,
                RETURN_INT,
                RETURN_FLOAT,
                RETURN_CLASS,
                OPERATOR,
                EXPRESSION_INT,
                EXPRESSION_FLOAT,
                EMPTY
            }
            public enum Cardinality
            {
                UNARY,
                BINARY,
                NA
            }

            private ElementType _type;
            private Cardinality _card;
            private object _value;
            private string _class_name;
            private Token _associated_token;

            public Token AssociatedToken
            {
                get
                {
                    return _associated_token;
                }
            }
            public string ClassName
            {
                get
                {
                    return _class_name;
                }
            }
            internal ElementType Type
            {
                get
                {
                    return _type;
                }

                set
                {
                    _type = value;
                }
            }

            public object Value
            {
                get
                {
                    return _value;
                }

                set
                {
                    _value = value;
                }
            }

            internal Cardinality Card
            {
                get
                {
                    return _card;
                }

                set
                {
                    _card = value;
                }
            }

            public ExpressionStackElement(ElementType t, Cardinality c, object v) : this(t, null, c, v, null)
            {

            }

            public ExpressionStackElement(ElementType t, Cardinality c, object v, Token assToken) : this(t,null,c,v, assToken)
            {

            }
            public ExpressionStackElement(ElementType t, string cn, Cardinality c, object v, Token assToken)
            {
                _associated_token = assToken;
                _type = t;
                _value = v;
                _card = c;
                _class_name = cn;
            }
        }

        private string _class_name;
        private ExpressionType _exp_type;
        private ExpressionValueType _val_type;
        private Stack<ExpressionStackElement> _expression_stack;

        public Stack<ExpressionStackElement> ExpressionStack
        {
            get
            {
                return _expression_stack;
            }
            
        }

        public string ClassName
        {
            get
            {
                return _class_name;
            }
        }
        public ExpressionValueType ValType
        {
            get
            {
                return _val_type;
            }
        }

        public Expression()
        {
            _exp_type = ExpressionType.UNDEFINED;
            _val_type = ExpressionValueType.UNDEFINED;
            _expression_stack = new Stack<ExpressionStackElement>();
        }

        public bool AddExpressionElement(ExpressionStackElement ese)
        {
            bool res = true;
            if (ese.Type == ExpressionStackElement.ElementType.OPERATOR)
            {
                if (_exp_type == ExpressionType.UNDEFINED)
                {
                    if (ese.Card == ExpressionStackElement.Cardinality.BINARY)
                    {
                        string val = (string)ese.Value;
                        if (val == "+" || val == "-" || val == "*" || val == "/")
                        {
                            _exp_type = ExpressionType.ARITHMETIC;
                        }
                        else
                        {
                            _exp_type = ExpressionType.RELATIONAL;
                            _val_type = ExpressionValueType.INTEGER;
                        }
                    }
                }
            }
            else
            {
                if (_val_type == ExpressionValueType.UNDEFINED)
                {
                    _val_type = GetTypeFromStackElement(ese);
                    if(_val_type == ExpressionValueType.CLASS)
                    {
                        _class_name = ese.ClassName;
                    }
                }
                if (GetTypeFromStackElement(ese) == ExpressionValueType.INTEGER && _val_type == ExpressionValueType.FLOAT
                    || GetTypeFromStackElement(ese) == ExpressionValueType.FLOAT && _val_type == ExpressionValueType.INTEGER
                    || GetTypeFromStackElement(ese) == ExpressionValueType.CLASS && _val_type != ExpressionValueType.CLASS
                    || GetTypeFromStackElement(ese) == ExpressionValueType.CLASS && _val_type == ExpressionValueType.CLASS && ese.ClassName != _class_name)
                {
                    //error
                    res = false;
                }
            }
            _expression_stack.Push(ese);
            return res;
        }
        public ExpressionValueType GetTypeFromStackElement(ExpressionStackElement ese)
        {
            if (ese.Type == ExpressionStackElement.ElementType.VARIABLE_INT ||
                ese.Type == ExpressionStackElement.ElementType.RETURN_INT ||
                ese.Type == ExpressionStackElement.ElementType.EXPRESSION_INT ||
                ese.Type == ExpressionStackElement.ElementType.IMMEDIATE_INT ||
                ese.Type == ExpressionStackElement.ElementType.VARIABLE_INT_ARRAY)
            {
                return ExpressionValueType.INTEGER;
            }
            else if(ese.Type == ExpressionStackElement.ElementType.RETURN_CLASS ||
                ese.Type == ExpressionStackElement.ElementType.VARIABLE_CLASS)
            {
                return ExpressionValueType.CLASS;
            }
            else 
            {
                return ExpressionValueType.FLOAT;
            }
        }

      

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach(var ese in _expression_stack)
            {
                sb.Append(ese.Value.ToString());
            }
            return sb.ToString();
        }
    }
}

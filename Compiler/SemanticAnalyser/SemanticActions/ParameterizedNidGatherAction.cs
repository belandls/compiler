﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class ParameterizedNidGatherAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction temp = sa.SemanticStack.Pop();
            NestedId tempNid = (NestedId)temp.SemanticValue;

            ParameterizedNestedId pnid = new ParameterizedNestedId(tempNid);

            SemanticValue = pnid;
        }
    }
}

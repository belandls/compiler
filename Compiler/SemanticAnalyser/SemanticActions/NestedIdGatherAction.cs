﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class NestedIdGatherAction : SemanticAction
    {
       
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction temp = sa.SemanticStack.Pop();
            DimensionnedId id = (DimensionnedId)temp.SemanticValue;
            NestedId nid = new NestedId(id, nextToken);
            //SymbolEntry se = sa.CurrentTable.LookupByNameAndType(idName, SymbolEntry.KindOfSymbol.VARIABLE | SymbolEntry.KindOfSymbol.PARAMETER);
            //if (se != null)
            //{
            //    nid = new NestedId(se);
            //}
            //else
            //{
            //    sa.Errors.Add(new SemanticError(SemanticError.ErrorType.UNDECLARED_VAR, nextToken));
            //}
            SemanticValue = nid;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class GenerateReturnStatementAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            SemanticAction temp = sa.SemanticStack.Pop();
            Expression exp = (Expression)temp.SemanticValue;

            if(sa.CurrentPass == Pass.SECOND)
            {
                var se = sa.CurrentTable.Parent.LookupByNameAndType(sa.CurrentTable.Name, SymbolEntry.KindOfSymbol.FUNCTION, true);
                Types retType = se.Types[0].Type;
                if (retType == Types.INT && exp.ValType != Expression.ExpressionValueType.INTEGER ||
                    retType == Types.FLOAT && exp.ValType != Expression.ExpressionValueType.FLOAT ||
                    retType == Types.CLASS && exp.ValType != Expression.ExpressionValueType.CLASS ||
                    retType == Types.CLASS && exp.ValType != Expression.ExpressionValueType.CLASS && se.Types[0].ClassName != exp.ClassName)
                {
                    sa.Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_RETURN_TYPE, nextToken, se.Types[0].Type));
                }
                sa.Generator.GenerateFunctionReturn(exp, sa.CurrentTable);

            }
            else
            {
                //generate return
            }

        }
    }
}

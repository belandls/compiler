﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class DimensionnedIdAggregationAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction exp = sa.SemanticStack.Pop();
            SemanticAction did = sa.SemanticStack.Pop();
            DimensionnedId val = (DimensionnedId)did.SemanticValue;

            val.Dimensions.Add((Expression)exp.SemanticValue);
            SemanticValue = val;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class DimensionAggregationAction : SemanticAction
    {


        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction dimAction = sa.SemanticStack.Pop();
            SemanticAction tidAction = sa.SemanticStack.Pop();
            TypedId tid = (TypedId)tidAction.SemanticValue;
            tid.Dimensions.Add((int)dimAction.SemanticValue);
            SemanticValue = tid;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class CreateFnTableAction : SemanticAction
    {

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            SemanticAction action = sa.SemanticStack.Pop();
            ParameterizedTypedId ptid = (ParameterizedTypedId)action.SemanticValue;

            sa.CreateTable(ptid.IdName, SymbolTable.TableType.FUNCTION);

            if (sa.CurrentPass == Pass.FIRST)
            {
                if (!sa.CurrentTable.Parent.CreateFunctionEntry(ptid, sa.CurrentTable))
                {
                    sa.Errors.Add(new SemanticError(SemanticError.ErrorType.MULTIPLY_DEFINED, ptid.AssociatedToken));
                }
                
            }
            else
            {
                sa.AttachCurrentTable();
            }

            sa.CurrentTable.CreateRetValEntry(ptid);
            foreach (TypedId tid in ptid.Parameters)
            {
                if (!sa.CurrentTable.CreateParameterEntry(tid))
                {
                    sa.Errors.Add(new SemanticError(SemanticError.ErrorType.MULTIPLY_DEFINED, tid.AssociatedToken));
                }
            }
            sa.CurrentTable.GenerateStackOffsets();

        }
    }
}

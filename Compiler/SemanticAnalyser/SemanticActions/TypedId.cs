﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    public class TypedId : DimensionnedType
    {
        private bool _resolved;
        private string _id_name;
        private Token _type_Token;
        public string IdName
        {
            get
            {
                return _id_name;
            }
        }

        public Token TypeToken
        {
            get
            {
                return _type_Token;
            }
        }

        public bool Resolved
        {
            get
            {
                return _resolved;
            }

            set
            {
                _resolved = value;
            }
        }

        public TypedId(Types type, string idName, Token assToken, Token typeToken) : this(type, null, idName, new List<int>(), assToken, typeToken) { }
        public TypedId(string className, string idName, Token assToken, Token typeToken) : this(Types.CLASS, className,idName, new List<int>(), assToken, typeToken) { }
        public TypedId(Types type, string className, string idName, List<int> dimensions, Token assToken, Token typeToken) : base(type, className, dimensions, assToken)
        {
            _id_name = idName;
            _type_Token = typeToken;
            _resolved = false;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class ExpressionElementGatherAction : SemanticAction
    {

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            Expression.ExpressionStackElement e = null;
            switch (nextToken.Type)
            {
                case TokenType.T_OR:
                case TokenType.T_ASSIGNMENT:
                case TokenType.T_MULTIPLICATION:
                case TokenType.T_DIVISION:
                case TokenType.T_EQUALS:
                case TokenType.T_GREATER:
                case TokenType.T_LESS:
                case TokenType.T_LESS_EQUAL:
                case TokenType.T_GREATER_EQUAL:
                case TokenType.T_N_EQUALS:
                case TokenType.T_AND:
                    e = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.OPERATOR, Expression.ExpressionStackElement.Cardinality.BINARY, nextToken.Value, nextToken);
                    break;
                case TokenType.T_NUM_INTEGER:
                    e = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.IMMEDIATE_INT, Expression.ExpressionStackElement.Cardinality.NA, nextToken.Value, nextToken);
                    break;
                case TokenType.T_NUM_FLOAT:
                    e = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.IMMEDIATE_FLOAT, Expression.ExpressionStackElement.Cardinality.NA, nextToken.Value, nextToken);
                    break;
                case TokenType.T_PLUS:
                case TokenType.T_MINUS:
                case TokenType.T_NOT:
                    if (sa.SemanticStack.Count == 0 ||
                        sa.SemanticStack.Peek().SemanticValue.GetType() != typeof(Expression.ExpressionStackElement) ||
                        ((Expression.ExpressionStackElement)sa.SemanticStack.Peek().SemanticValue).Type == Expression.ExpressionStackElement.ElementType.OPERATOR)
                    {
                        e = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.OPERATOR, Expression.ExpressionStackElement.Cardinality.UNARY, nextToken.Value, nextToken);
                    }
                    else
                    {
                        e = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.OPERATOR, Expression.ExpressionStackElement.Cardinality.BINARY, nextToken.Value, nextToken);
                    }
                    break;
                case TokenType.T_RETURN:
                    e = new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.OPERATOR, Expression.ExpressionStackElement.Cardinality.UNARY, nextToken.Value, nextToken);
                    break;
            }
            SemanticValue = e;
        }
    }
}

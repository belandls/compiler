﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    public class NestedId
    {
        private Queue<DimensionnedId> _ids;
        private DimensionnedId _initial;
        private Token _associated_token;

        public NestedId(DimensionnedId init, Token assToken)
        {
            _initial = init;
            _associated_token = assToken;
            _ids = new Queue<DimensionnedId>();
        }

        public Queue<DimensionnedId> Ids
        {
            get
            {
                return _ids;
            }

        }

        internal DimensionnedId Initial
        {
            get
            {
                return _initial;
            }
        }

        public Token Associated_token
        {
            get
            {
                return _associated_token;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(_initial.ToString());
            foreach(var nest in _ids)
            {
                sb.Append('.').Append(nest.ToString());
            }
            return sb.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class DimensionnedIdGatherAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction temp = sa.SemanticStack.Pop();
            string idName = (string)temp.SemanticValue;
            SemanticValue = new DimensionnedId(idName, nextToken);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class GatherDimensionAction : SemanticAction
    {
     

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            int size = -1;
            int.TryParse(nextToken.Value, out size);
            SemanticValue = size;
        }
    }
}

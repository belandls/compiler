﻿using Compiler.SyntaxAnalyserComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    public abstract class SemanticAction 
    {
        object _semantic_value;
        Token _associated_token;
        bool _should_stack;

        public object SemanticValue
        {
            get
            {
                return _semantic_value;
            }
            set
            {
                _semantic_value = value;
            }
        }

        public bool ShouldStack
        {
            get
            {
                return _should_stack;
            }

            set
            {
                _should_stack = value;
            }
        }

        public Token AssociatedToken
        {
            get
            {
                return _associated_token;
            }

            set
            {
                _associated_token = value;
            }
        }

        public SemanticAction() 
        {
            _should_stack = true;
        }

        public abstract void Execute(SemanticAnalyser sa, Token nextToken);

        //public override string ToString()
        //{
        //    return this.Value;
        //}
    }
}

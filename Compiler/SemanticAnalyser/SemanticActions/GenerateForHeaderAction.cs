﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class GenerateForHeaderAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            if(sa.CurrentPass == Pass.SECOND && !sa.HasErrors)
            {
                ShouldStack = true;
                SemanticValue = sa.SemanticStack.Pop().SemanticValue; //For footer
                SemanticAction condTemp = sa.SemanticStack.Pop();
                SemanticAction initTemp = sa.SemanticStack.Pop();

                sa.Generator.GenerateForBeginning((Expression)initTemp.SemanticValue, (Expression)condTemp.SemanticValue, sa.CurrentTable);
            }
        }
    }
}

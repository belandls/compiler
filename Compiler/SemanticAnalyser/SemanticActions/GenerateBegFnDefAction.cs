﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class GenerateBegFnDefAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            if(sa.CurrentPass == Pass.SECOND)
            {
                ShouldStack = true;
                SemanticAction temp = sa.SemanticStack.Pop();
                SemanticValue = temp.SemanticValue;
                sa.Generator.GenerateBegFunctionDef((string)temp.SemanticValue,sa.CurrentTable.LookupByNameAndType((string)SemanticValue,SymbolEntry.KindOfSymbol.FUNCTION).LinkedTable);
            }
        }
    }
}

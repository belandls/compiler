﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class GenerateExpressionAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            if (sa.CurrentPass == Pass.SECOND && !sa.HasErrors && !sa.HasErrors)
            {
                SemanticAction temp = sa.SemanticStack.Pop();
                sa.Generator.GenerateExpression((Expression)temp.SemanticValue, sa.CurrentTable);
            }
        }
    }
}

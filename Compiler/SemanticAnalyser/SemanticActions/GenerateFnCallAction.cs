﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class GenerateFnCallAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            if(sa.CurrentPass == Pass.SECOND && sa.Errors.Count == 0)
            {
                ShouldStack = true;
                SemanticAction temp = sa.SemanticStack.Pop();
                SemanticValue = temp.SemanticValue;
                ParameterizedNestedId pnid = (ParameterizedNestedId)((Expression.ExpressionStackElement)temp.SemanticValue).Value;
                sa.Generator.GenerateFunctionCall(pnid, sa.CurrentTable);
            }
        }
    }
}

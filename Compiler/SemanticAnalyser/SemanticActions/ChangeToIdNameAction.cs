﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class ChangeToIdNameAction : SemanticAction
    {

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction temp = sa.SemanticStack.Pop();
            Tuple<Types, string, Token> val = (Tuple<Types, string, Token>)temp.SemanticValue;
            SemanticValue = val.Item2;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class CreateRootTableAction : SemanticAction
    {

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            if(sa.CurrentPass == Pass.FIRST)
            {
                sa.CreateTable("ROOT", SymbolTable.TableType.ROOT);
            }
            else
            {
                sa.Generator.GenerateEntryPoint();
                sa.SelectTable("ROOT", SymbolTable.TableType.ROOT);
            }
            
        }
    }
}

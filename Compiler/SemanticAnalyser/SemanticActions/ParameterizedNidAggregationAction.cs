﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class ParameterizedNidAggregationAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction expTemp = sa.SemanticStack.Pop();
            SemanticAction pnidTemp = sa.SemanticStack.Pop();
            Expression exp = (Expression)expTemp.SemanticValue;
            ParameterizedNestedId pnid = (ParameterizedNestedId)pnidTemp.SemanticValue;

            pnid.Params.Add(exp);
            SemanticValue = pnid;
        }
    }
}

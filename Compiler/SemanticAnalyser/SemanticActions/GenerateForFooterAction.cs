﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class GenerateForFooterAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            if (sa.CurrentPass == Pass.SECOND && !sa.HasErrors)
            {
                SemanticAction incrTemp = sa.SemanticStack.Pop();
                sa.Generator.GenerateForEnd((Expression)incrTemp.SemanticValue, sa.CurrentTable);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class ParameterizedNestedId
    {
        private NestedId _nid;
        private List<Expression> _params;

        public List<Expression> Params
        {
            get
            {
                return _params;
            }

            set
            {
                _params = value;
            }
        }

        public NestedId Nid
        {
            get
            {
                return _nid;
            }
        }

        public ParameterizedNestedId(NestedId nid)
        {
            _nid = nid;
            _params = new List<Expression>();
        }

    }
}

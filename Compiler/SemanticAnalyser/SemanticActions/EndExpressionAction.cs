﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class EndExpressionAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            Expression.ExpressionStackElement aggregatedEse = (Expression.ExpressionStackElement)sa.SemanticStack.Pop().SemanticValue;
            Expression e = (Expression)sa.SemanticStack.Pop().SemanticValue;

            e.AddExpressionElement(aggregatedEse);
           
            //Expand

            SemanticValue = e;
        }
    }
}

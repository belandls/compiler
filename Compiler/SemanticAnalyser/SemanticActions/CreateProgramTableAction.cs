﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class CreateProgramTableAction : SemanticAction
    {

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            ParameterizedTypedId ptid = new ParameterizedTypedId(Types.VOID, "PROGRAM", null, null);
            sa.CreateTable(ptid.IdName, SymbolTable.TableType.FUNCTION);
            if (sa.CurrentPass == Pass.FIRST)
            {
                sa.CurrentTable.Parent.CreateFunctionEntry(ptid, sa.CurrentTable);
            }
            else
            {
                sa.AttachCurrentTable();
            }
            
        }
    }
}

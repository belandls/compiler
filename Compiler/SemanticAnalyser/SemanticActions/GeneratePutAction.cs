﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class GeneratePutAction : SemanticAction
    {
        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            if (sa.CurrentPass == Pass.SECOND && !sa.HasErrors)
            {
                SemanticAction temp = sa.SemanticStack.Pop();
                sa.Generator.GeneratePut((Expression)temp.SemanticValue,sa.CurrentTable);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class CreateForTableAction : SemanticAction
    {

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            sa.CreateTable("FOR", SymbolTable.TableType.FOR_SCOPE);
            sa.CurrentTable.Parent.CreateForEntry(sa.CurrentTable);
            //if(sa.CurrentPass == Pass.FIRST)
            //{
            //    sa.CurrentTable.Parent.CreateForEntry(sa.CurrentTable);
            //}
            //else
            //{
            //    sa.AttachCurrentForTable();
            //}

        }
    }
}

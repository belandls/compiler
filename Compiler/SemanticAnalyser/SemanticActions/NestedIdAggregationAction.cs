﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class NestedIdAggregationAction : SemanticAction
    {

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            SemanticAction idgAction = sa.SemanticStack.Pop();
            SemanticAction temp = sa.SemanticStack.Pop();
            DimensionnedId idName = (DimensionnedId)idgAction.SemanticValue;
            NestedId nid = (NestedId)temp.SemanticValue;
            nid.Ids.Enqueue(idName);
            SemanticValue = nid;
        }
    }
}

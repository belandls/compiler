﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    class CreateFunctionAccessAction : SemanticAction
    {

        public override void Execute(SemanticAnalyser sa, Token nextToken)
        {
            ShouldStack = false;
            SemanticAction temp = sa.SemanticStack.Pop();
            if (temp.SemanticValue != null) //If undeclared, gonna null
            {
                sa.CreateFunctionAccess((NestedId)temp.SemanticValue);
            }

        }
    }
}

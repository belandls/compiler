﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Compiler.SemanticAnalyser.SemanticActions;
using Compiler.SyntaxAnalyserComponent;
using Compiler.Code_Generation;

namespace Compiler.SemanticAnalyser
{
    public enum Pass
    {
        FIRST,
        SECOND
    }
    public class SemanticAnalyser
    {
        private Pass _current_pass;
        private Stack<SemanticAction> _semantic_stack;
        private SymbolTable _current_table;
        private SymbolTable _root;
        private List<SemanticError> _errors;
        private List<SymbolEntry> _unresolved;
        private List<Tuple<SymbolTable, NestedId>> _var_access;
        private List<Tuple<SymbolTable, NestedId>> _func_access;
        private List<SymbolEntry> _recursive_check;
        private CodeGenerator _generator;

        public bool HasErrors
        {
            get
            {
                return _errors.Count > 0;
            }
        }
        public Pass CurrentPass
        {
            get
            {
                return _current_pass;
            }
            set
            {
                _current_pass = value;
            }
        }
        public Stack<SemanticAction> SemanticStack
        {
            get
            {
                return _semantic_stack;
            }
        }

        public SymbolTable CurrentTable
        {
            get
            {
                return _current_table;
            }
            set
            {
                _current_table = value;
            }

        }

        public List<SemanticError> Errors
        {
            get
            {
                return _errors;
            }
        }

        internal CodeGenerator Generator
        {
            get
            {
                return _generator;
            }
        }

        public SemanticAnalyser(string outName)
        {
            _current_pass = Pass.FIRST;
            _semantic_stack = new Stack<SemanticAction>();
            _errors = new List<SemanticError>();
            _unresolved = new List<SymbolEntry>();
            _recursive_check = new List<SymbolEntry>();
            _var_access = new List<Tuple<SymbolTable, NestedId>>();
            _func_access = new List<Tuple<SymbolTable, NestedId>>();
            _generator = new CodeGenerator(outName);
        }

        public void HandleSemanticAction(SemanticActionFactory saf, Token nextToken)
        {
            SemanticAction sa = saf.GeneratedAction;
            sa.Execute(this, nextToken);
            if (sa.ShouldStack)
            {
                _semantic_stack.Push(sa);
            }
        }

        public void CreateTable(string name, SymbolTable.TableType type)
        {
            SymbolTable temp;
            if (type == SymbolTable.TableType.ROOT)
            {
                temp = new SymbolTable(name, type, null);
                _root = temp;
            }
            else
            {
                temp = new SymbolTable(name, type, CurrentTable);
            }
            _current_table = temp;
        }

        public void AttachCurrentTable()
        {
            var se = _current_table.LookupByNameAndType(_current_table.Name, SymbolEntry.KindOfSymbol.FUNCTION);
            se.LinkedTable = _current_table;
        }
        public void AttachCurrentForTable()
        {
            var se = _current_table.LookupByNameAndType(_current_table.Name, SymbolEntry.KindOfSymbol.FOR_SCOPE);
            se.LinkedTable = _current_table;
        }

        public void SelectTable(string name, SymbolTable.TableType tt)
        {
            switch (tt)
            {
                case SymbolTable.TableType.ROOT:
                    _current_table = _root;
                    break;
                case SymbolTable.TableType.CLASS:
                    var se1 = _current_table.LookupByNameAndType(name, SymbolEntry.KindOfSymbol.CLASS);
                    _current_table = se1.LinkedTable;
                    break;
                case SymbolTable.TableType.FUNCTION:
                    var se2 = _current_table.LookupByNameAndType(name, SymbolEntry.KindOfSymbol.FUNCTION);
                    _current_table = se2.LinkedTable;
                    break;
            }
        }

        public void CreateVarAccess(NestedId nid)
        {
            _var_access.Add(new Tuple<SymbolTable, NestedId>(_current_table, nid));
        }

        public void CreateFunctionAccess(NestedId nid)
        {
            _func_access.Add(new Tuple<SymbolTable, NestedId>(_current_table, nid));
        }

        public void EndCurrentScope()
        {
            _unresolved.AddRange(_current_table.Unresolved);
            _current_table = _current_table.Parent;
        }

        public void CheckForRecursiveMemAlloc(TypedId tid)
        {
            if (tid.Type != Types.CLASS || !tid.Resolved)
                return;
            if (IsRecusiveAllocation(new HashSet<string>(), _root.LookupByNameAndType(tid.ClassName, SymbolEntry.KindOfSymbol.CLASS).LinkedTable, tid.ClassName))
            {
                Errors.Add(new SemanticError(SemanticError.ErrorType.RECURSIVE, (tid.AssociatedToken)));
            }
        }
        private bool IsRecusiveAllocation(HashSet<string> visited, SymbolTable currentTable, string toCheck)
        {
            var possibleClass = currentTable.Entries.Where((e) => e.Types[0].Type == Types.CLASS).ToList();
            if (possibleClass.Count > 0)
            {
                if (visited.Contains(currentTable.Name))
                {
                    return false;
                }
                else
                {

                    visited.Add(currentTable.Name);
                    bool res = false;
                    foreach (var pc in possibleClass)
                    {
                        if (toCheck == pc.Types[0].ClassName)
                        {
                            return true;
                        }
                        SymbolTable st = _root.LookupByNameAndType(pc.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS).LinkedTable;
                        res = IsRecusiveAllocation(visited, st, toCheck);
                        if (res)
                            break;
                    }
                    return res;
                }
            }
            else
            {
                return false;
            }
        }

        public void ResoveIdentifiers()
        {
            //foreach (var se in _unresolved)
            //{
            //    if (se.Kind == SymbolEntry.KindOfSymbol.VARIABLE || se.Kind == SymbolEntry.KindOfSymbol.PARAMETER)
            //    {
            //        if (_root.LookupByNameAndType(se.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS) == null)
            //        {
            //            _errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_TYPE, ((TypedId)se.Types[0]).TypeToken));
            //        }
            //        else
            //        {
            //            _recursive_check.Add(se);
            //        }
            //    }
            //    else if (se.Kind == SymbolEntry.KindOfSymbol.FUNCTION)
            //    {
            //        if (_root.LookupByNameAndType(se.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS) == null)
            //        {
            //            _errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_TYPE, ((TypedId)se.Types[0]).TypeToken));
            //        }
            //    }

            //}
        }

        public void ResolveVarAccess()
        {

            //foreach (var nid in _var_access)
            //{
            //    SymbolEntry se = nid.Item1.LookupByNameAndType(nid.Item2.Initial, SymbolEntry.KindOfSymbol.VARIABLE | SymbolEntry.KindOfSymbol.PARAMETER);
            //    if (se == null)
            //    {
            //        Errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_VAR, nid.Item2.Associated_token));
            //    }
            //    else
            //    {
            //        if (nid.Item2.Ids.Count > 0)
            //        {
            //            //SymbolTable t = nid.Item1;

            //            SymbolTable t = _root.LookupByNameAndType(se.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS).LinkedTable;
            //            DimensionnedId did;
            //            while (nid.Item2.Ids.Count > 0)
            //            {
            //                did = nid.Item2.Ids.Dequeue();
            //                SymbolEntry seVar = t.LookupByNameAndType(did.IdName, SymbolEntry.KindOfSymbol.VARIABLE);
            //                if (seVar != null)
            //                {
            //                    if (nid.Item2.Ids.Count > 0)
            //                    {
            //                        SymbolEntry seCla = _root.LookupByNameAndType(seVar.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS);
            //                        if (seCla == null)
            //                        {
            //                            Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_NESTING, did.AssociatedToken));
            //                            break;
            //                        }
            //                        else
            //                        {
            //                            t = seCla.LinkedTable;
            //                        }

            //                    }
            //                }
            //                else
            //                {
            //                    Errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_VAR, did.AssociatedToken));
            //                    break;
            //                }
            //            }
            //        }
            //        else
            //        {
            //            //For code generation
            //        }
            //    }

            //}
        }

        public void ResolveFuncAccess()
        {
            //foreach (var nid in _func_access)
            //{
            //    if (nid.Item2.Ids.Count == 0)
            //    {
            //        SymbolEntry se = nid.Item1.LookupByNameAndType(nid.Item2.Initial, SymbolEntry.KindOfSymbol.FUNCTION);
            //        if (se == null)
            //        {
            //            Errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_FN, nid.Item2.Associated_token));
            //        }
            //    }
            //    else
            //    {
            //        SymbolEntry se = nid.Item1.LookupByNameAndType(nid.Item2.Initial, SymbolEntry.KindOfSymbol.VARIABLE | SymbolEntry.KindOfSymbol.PARAMETER);
            //        if (se == null)
            //        {
            //            Errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_VAR, nid.Item2.Associated_token));
            //        }
            //        else
            //        {
            //            SymbolTable t = _root.LookupByNameAndType(se.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS).LinkedTable;
            //            //SymbolTable t = _root.LookupByNameAndType(nid.Item2.Initial.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS).LinkedTable;
            //            bool incompletePath = false;
            //            DimensionnedId did;
            //            while (nid.Item2.Ids.Count > 1)
            //            {
            //                did = nid.Item2.Ids.Dequeue();
            //                SymbolEntry seVar = t.LookupByNameAndType(did.IdName, SymbolEntry.KindOfSymbol.VARIABLE);
            //                if (seVar != null)
            //                {
            //                    if (nid.Item2.Ids.Count > 0)
            //                    {
            //                        SymbolEntry seCla = _root.LookupByNameAndType(seVar.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS);
            //                        if (seCla == null)
            //                        {
            //                            Errors.Add(new SemanticError(SemanticError.ErrorType.INVALID_NESTING, did.AssociatedToken));
            //                            incompletePath = true;
            //                            break;
            //                        }
            //                        else
            //                        {
            //                            t = seCla.LinkedTable;
            //                        }

            //                    }
            //                }
            //                else
            //                {
            //                    Errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_VAR, did.AssociatedToken));
            //                    //Console.WriteLine("Var doesn't exist");
            //                    incompletePath = true;
            //                    break;
            //                }
            //            }
            //            if (!incompletePath)
            //            {
            //                did = nid.Item2.Ids.Dequeue();
            //                SymbolEntry se2 = t.LookupByNameAndType(did.IdName, SymbolEntry.KindOfSymbol.FUNCTION);
            //                if (se2 == null)
            //                {
            //                    Errors.Add(new SemanticError(SemanticError.ErrorType.UNDEFINED_FN, did.AssociatedToken));
            //                }
            //            }
            //        }
            //    }
            //    //SymbolEntry se = _root.LookupByNameAndType(nid.Item2.Initial, SymbolEntry.KindOfSymbol.FUNCTION );
            //    //if (se == null)
            //    //{
            //    //    Console.WriteLine("lawl2");
            //    //}
            //    //else
            //    //{



            //    //}

            //}
        }

        public string GetTablesOutput()
        {
            StringBuilder sb = new StringBuilder();
            DumpTables(_root, ref sb, "");
            return sb.ToString();
        }

        private void DumpTables(SymbolTable st, ref StringBuilder sb, string ident)
        {
            //sb.Append(ident);
            sb.Append(ident).Append("===").Append(st.Name).Append(":").Append(st.Type.ToString()).AppendLine("===");
            foreach (SymbolEntry se in st.Entries)
            {
                sb.Append(ident).Append('(').Append(st.Name).Append(')').Append(" -> ").AppendLine(se.ToString());
                if (se.LinkedTable != null)
                {
                    ident += '\t';
                    DumpTables(se.LinkedTable, ref sb, ident);
                    ident = ident.Remove(ident.Length - 1);
                }

            }
            if (st.Entries.Count == 0)
            {
                sb.Append(ident).AppendLine("< EMPTY SYMBOL TABLE >");
            }
        }

        public void DumpErrors()
        {
            foreach (var se in _errors)
            {
                Console.WriteLine(se.ToString());
            }
        }
    }
}

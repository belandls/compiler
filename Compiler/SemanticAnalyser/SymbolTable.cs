﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    public class SymbolTable
    {

        public enum TableType
        {
            CLASS,
            FUNCTION,
            FOR_SCOPE,
            ROOT
        }

        private SymbolTable _parent;
        private List<SymbolEntry> _entries;
        private string _name;
        private TableType _type;
        private int _size;
        private List<SymbolEntry> _unresolved;

        public string Name
        {
            get
            {
                return _name;
            }
        }

        internal SymbolTable Parent
        {
            get
            {
                return _parent;
            }
        }

        internal List<SymbolEntry> Unresolved
        {
            get
            {
                return _unresolved;
            }
        }

        public List<SymbolEntry> Entries
        {
            get
            {
                return _entries;
            }
        }
        public TableType Type
        {
            get
            {
                return _type;
            }
        }

        public int Size
        {
            get
            {
                return _size;
            }

        }

        public SymbolTable GetRoot()
        {
            SymbolTable currentLookupTable = this;
            bool reachedRoot = false;
            do
            {
                reachedRoot = currentLookupTable.Parent == null;
                if (!reachedRoot)
                {
                    currentLookupTable = currentLookupTable.Parent;
                }
                
            } while (!reachedRoot);
            return currentLookupTable;
        }

        public SymbolTable(string name, TableType type, SymbolTable parent)
        {
            _entries = new List<SymbolEntry>();
            _name = name;
            _type = type;
            _parent = parent;
            _unresolved = new List<SymbolEntry>();
            _size = 0;
        }

        public bool IsRecursiveInstantiation(TypedId tid)
        {
            if (_type == TableType.CLASS)
            {
                if (tid.Type == Types.CLASS)
                {
                    if (tid.ClassName == _name)
                    {
                        return true;
                    }
                    else
                    {
                        SymbolTable root = this;
                        while(root.Parent != null)
                        {
                            root = root.Parent;
                        }
                        return FindRecursiveCall(new HashSet<string>(), root, tid.ClassName);
                    }
                }
            }
            return false;
        }
        public bool FindRecursiveCall(HashSet<string> visited, SymbolTable nextTable, string toCheck)
        {
            var possibleClass = nextTable._entries.Where((e) => e.Kind == SymbolEntry.KindOfSymbol.CLASS).ToList();
            if (possibleClass.Count > 0)
            {
                if (visited.Contains(nextTable.Name))
                {
                    return false;
                }
                else
                {
                    if (toCheck == nextTable.Name)
                    {
                        return true;
                    }
                    visited.Add(nextTable._name);
                    bool res = false;
                    foreach(var pc in possibleClass)
                    {
                        res = FindRecursiveCall(visited, pc.LinkedTable, toCheck);
                        if (res)
                            break;
                    }
                    return res;
                }
            }
            else
            {
                return false;
            }
        }

        public void GenerateStackOffsets()
        {
            int currentOffset = GetCurrentTableOffset();
            foreach(var e in _entries)
            {
                e.Offset = currentOffset;
                int tempOffset = 0;
                if(e.Types[0].Type == Types.CLASS)
                {
                    var se = LookupByNameAndType(e.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS);
                    tempOffset = se.LinkedTable.Size;
                }
                else
                {
                    tempOffset = 4;
                }
                foreach(var d in e.Types[0].Dimensions)
                {
                    tempOffset *= d;
                }
                currentOffset += tempOffset;
            }
        }

        public int GetStackOffsetForVar(NestedId nid)
        {
            int offset = 0;
            SymbolTable currentTable = this;
            SymbolEntry currentEntry = null;
            currentEntry = LookupByNameAndType(nid.Initial.IdName, SymbolEntry.KindOfSymbol.PARAMETER | SymbolEntry.KindOfSymbol.VARIABLE | SymbolEntry.KindOfSymbol.RETURN_VAL);
            offset += currentEntry.Offset;
            if (currentEntry.Types[0].Type == Types.CLASS)
            {
                currentTable = LookupByNameAndType(currentEntry.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS).LinkedTable;
            }
            foreach (var id in nid.Ids)
            {
                currentEntry = currentTable.LookupByNameAndType(nid.Initial.IdName, SymbolEntry.KindOfSymbol.PARAMETER | SymbolEntry.KindOfSymbol.VARIABLE | SymbolEntry.KindOfSymbol.RETURN_VAL);
                offset += currentEntry.Offset;
                if(currentEntry.Types[0].Type == Types.CLASS)
                {
                    currentTable = currentTable.LookupByNameAndType(currentEntry.Types[0].ClassName, SymbolEntry.KindOfSymbol.CLASS).LinkedTable;
                }
            }
            //if(Type == TableType.FOR_SCOPE)
            //{
            //    offset += Parent.Size;
            //}
            return offset;
        }

        public bool CreateVariableEntry(TypedId tid, bool resolved)
        {
            bool res = true;
            if(_entries.Exists((e) => e.Name == tid.IdName && e.Kind == SymbolEntry.KindOfSymbol.VARIABLE))
            {
                res = false;
            }
            int sizeTemp = 0;
            if(tid.Type == Types.CLASS)
            {
                if (tid.Resolved)
                {
                    sizeTemp += LookupByNameAndType(tid.ClassName, SymbolEntry.KindOfSymbol.CLASS).LinkedTable.Size;
                }
            }
            else
            {
                sizeTemp += 4;
            }
            foreach(var d in tid.Dimensions)
            {
                sizeTemp *= d;
            }
            _size += sizeTemp;
            SymbolEntry se = new SymbolEntry(tid.IdName, SymbolEntry.KindOfSymbol.VARIABLE, tid, null);
            
            _entries.Add(se);
            if (!resolved)
            {
                Unresolved.Add(se);
            }
            return res;
        }
        public bool ResolveVariableEntry(TypedId tid)
        {
            var entry = _entries.Where(e => e.Name == tid.IdName && e.Types[0].Type == Types.CLASS).FirstOrDefault();
            var res = LookupByNameAndType(entry.Name, SymbolEntry.KindOfSymbol.CLASS);
            entry.Resolved = res == null;
            return res == null;
        }

        public void CreateRetValEntry(ParameterizedTypedId ptid)
        {
            int sizeTemp = 0;
            if (ptid.Type == Types.CLASS)
            {
                if (ptid.Resolved)
                {
                    sizeTemp += LookupByNameAndType(ptid.ClassName, SymbolEntry.KindOfSymbol.CLASS).LinkedTable.Size;
                }
            }
            else
            {
                sizeTemp += 4;
            }
            foreach (var d in ptid.Dimensions)
            {
                sizeTemp *= d;
            }
            _size += sizeTemp;
            SymbolEntry se = new SymbolEntry(ptid.IdName, SymbolEntry.KindOfSymbol.RETURN_VAL, ptid, null);
            _entries.Add(se);
        }

        public bool CreateParameterEntry(TypedId tid)
        {
            bool res = true;
            if (_entries.Exists((e) => e.Name == tid.IdName && e.Kind == SymbolEntry.KindOfSymbol.PARAMETER))
            {
                res = false;
            }
            int sizeTemp = 0;
            if (tid.Type == Types.CLASS)
            {
                if (tid.Resolved)
                {
                    sizeTemp += LookupByNameAndType(tid.ClassName, SymbolEntry.KindOfSymbol.CLASS).LinkedTable.Size;
                }
            }
            else
            {
                sizeTemp += 4;
            }
            foreach (var d in tid.Dimensions)
            {
                sizeTemp *= d;
            }
            _size += sizeTemp;
            SymbolEntry se = new SymbolEntry(tid.IdName, SymbolEntry.KindOfSymbol.PARAMETER, tid,  null);
            _entries.Add(se);
            if (tid.Type == Types.CLASS)
            {
                Unresolved.Add(se);
            }
            return res;
        }

        public bool CreateFunctionEntry(ParameterizedTypedId ptid, SymbolTable linkedTable)
        {
            bool res = true;
            if (_entries.Exists((e) => e.Name == ptid.IdName))
            {
                res = false;
            }
            List<DimensionnedType> typesTemp = new List<DimensionnedType>();
            typesTemp.Add(ptid);
            typesTemp.AddRange(ptid.Parameters.Cast<DimensionnedType>().ToList());
            SymbolEntry se = new SymbolEntry(ptid.IdName, SymbolEntry.KindOfSymbol.FUNCTION,typesTemp,  linkedTable);
            if(ptid.Type == Types.CLASS)
            {
                Unresolved.Add(se);
            }
            _entries.Add(se);
            return res;
        }

        public void CreateForEntry(SymbolTable linkedTable)
        {
            SymbolEntry se = new SymbolEntry("for_loop", SymbolEntry.KindOfSymbol.FOR_SCOPE,new DimensionnedType(Types.VOID,null), linkedTable);
            _entries.Add(se);
        }

        public bool CreateClassEntry(string name, SymbolTable linkedTable)
        {
            bool res = true;
            if (_entries.Exists((e) => e.Name == name && e.Kind == SymbolEntry.KindOfSymbol.CLASS))
            {
                res = false;
            }
            SymbolEntry se = new SymbolEntry(name, SymbolEntry.KindOfSymbol.CLASS, new DimensionnedType(Types.VOID, null), linkedTable);
            _entries.Add(se);
            return res;
        }

        public SymbolEntry LookupByNameAndType(string name, SymbolEntry.KindOfSymbol kind, bool forceLocal = false)
        {
            SymbolEntry temp = null;
            SymbolTable currentLookupTable = this;
            bool reachedRoot = forceLocal;
            do
            {
                temp = currentLookupTable._entries.Where((ent) => ent.Name == name && (ent.Kind & kind) > 0).FirstOrDefault();
                if(temp != null)
                {
                    break;
                }
                currentLookupTable = currentLookupTable.Parent;
                if (!forceLocal)
                {
                    reachedRoot = currentLookupTable == null;
                }
            } while (!reachedRoot);

            return temp;
        }

        public int GetCurrentTableOffset()
        {
            int offset = 0;
            SymbolTable currentLookupTable = this;
            bool reachedRoot = false;
            do
            {
                reachedRoot = currentLookupTable.Parent == null;
                if (!reachedRoot)
                {
                    currentLookupTable = currentLookupTable.Parent;
                    offset += currentLookupTable.Size;
                }
            } while (!reachedRoot);
            return offset;
        }

        public SymbolTable LookupNestedFunctionTable(NestedId nid)
        {
            if (nid.Ids.Count == 0)
            {
                var res = LookupByNameAndType(nid.Initial.IdName, SymbolEntry.KindOfSymbol.FUNCTION);
                if (res != null)
                {
                    return res.LinkedTable;
                }
            }
            return null;
        }

        //public string GetTableDump()
        //{
        //    StringBuilder sb = new StringBuilder();
        //    DumpTables()
        //    //sb.Append(_name).Append(":").AppendLine(_type.ToString());
        //    //foreach(SymbolEntry se in _entries)
        //    //{
        //    //    sb.AppendLine(se.ToString());
        //    //}
        //    return sb.ToString();
        //}

        

        public override string ToString()
        {
            return _name;
        }
    }
}

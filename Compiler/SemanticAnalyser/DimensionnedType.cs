﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser
{
    public enum Types
    {
        VOID,
        INT,
        FLOAT,
        CLASS
    }

    public class DimensionnedType : DimensionnedElement
    {
        private Types _type;
        private string _class_name;
        

        public Types Type
        {
            get
            {
                return _type;
            }
        }
        public string ClassName
        {
            get
            {
                return _class_name;
            }
        }
        


        public DimensionnedType(Types type, Token associatedToken) : this(type, null, new List<int>(), associatedToken) { }
        public DimensionnedType(string className, Token associatedToken) : this(Types.CLASS, className, new List<int>(), associatedToken) { }
        public DimensionnedType(Types type, string className, List<int> dimensions, Token associatedToken) : base(dimensions, associatedToken)
        {
            _type = type;
            _class_name = className;
        }

    }
}

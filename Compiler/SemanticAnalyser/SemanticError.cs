﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser
{
    public class SemanticError
    {
        public enum ErrorType
        {
            MULTIPLY_DEFINED,
            UNDEFINED_TYPE,
            UNDEFINED_FN,
            UNDEFINED_VAR,
            RECURSIVE,
            INVALID_NESTING,
            UNEXPECTED_TYPE_FN_PARAM,
            UNEXPECTED_PARAMETER_COUNT,
            UNEXPECTED_TYPE_EXP,
            INVALID_RETURN_TYPE,
            INVALID_INDICE_TYPE,
            INVALID_OP_ON_OBJ,
            INVALID_ARRAY_OP,
            INVALID_ARRAY_ACCESS
        }

        private ErrorType _type;
        private Token _associated_token;
        private object _add_info;

        public SemanticError(ErrorType type, Token associatedToken, object addInfo = null)
        {
            _type = type;
            _associated_token = associatedToken;
            _add_info = addInfo;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(_associated_token.ToString());
            switch (_type)
            {
                case ErrorType.MULTIPLY_DEFINED:
                    sb.Append("An identifier -").Append(_associated_token.Value).AppendLine("- is already defined in this scope.");
                    break;
                case ErrorType.RECURSIVE:
                    sb.Append("A variable of type -").Append(_associated_token.Value).AppendLine("- in this scope will lead to an infinite memory allocation loop.");
                    break;
                case ErrorType.UNDEFINED_TYPE:
                    sb.Append("The class -").Append(_associated_token.Value).AppendLine("- is undefined.");
                    break;
                case ErrorType.UNDEFINED_FN:
                    sb.Append("The function -").Append(_associated_token.Value).AppendLine("- is undefined.");
                    break;
                case ErrorType.UNDEFINED_VAR:
                    sb.Append("The variable -").Append(_associated_token.Value).AppendLine("- is not defined.");
                    break;
                case ErrorType.INVALID_NESTING:
                    sb.Append("The variable -").Append(_associated_token.Value).AppendLine("- is a primitive type, thus it cannot be nested after.");
                    break;
                case ErrorType.UNEXPECTED_TYPE_FN_PARAM:
                    Tuple<int, string, string> temp = (Tuple<int, string, string>)_add_info;
                    sb.Append("Cannot convert parameter ").Append(temp.Item1).Append(" from ").Append(temp.Item2).Append(" to ").Append(temp.Item3).Append('.');
                    break;
                case ErrorType.UNEXPECTED_PARAMETER_COUNT:
                    sb.Append("The function ").Append(_associated_token.Value).Append(" does not take ").Append((int)_add_info).Append(" parameters.");
                    break;
                case ErrorType.UNEXPECTED_TYPE_EXP:
                    sb.Append("Expression must be of type ").Append(_add_info.ToString());
                    break;
                case ErrorType.INVALID_RETURN_TYPE:
                    sb.Append("Return type is not of type ").Append(_add_info.ToString());
                    break;
                case ErrorType.INVALID_INDICE_TYPE:
                    sb.Append("Indices must be of type INTEGER");
                    break;
                case ErrorType.INVALID_OP_ON_OBJ:
                    sb.Append("Only assignment operations are allowed on objects");
                    break;
                case ErrorType.INVALID_ARRAY_OP:
                    sb.Append("Operations on arrays are not allowed");
                    break;
                case ErrorType.INVALID_ARRAY_ACCESS:
                    sb.Append("This dimension exceeds the avaible dimensions");
                    break;
            }
            return sb.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SemanticAnalyser.SemanticActions
{
    public class SymbolEntry
    {
        public enum KindOfSymbol
        {
            FUNCTION= 1,
            CLASS = 2,
            PARAMETER = 4,
            VARIABLE = 8,
            FOR_SCOPE = 16,
            RETURN_VAL = 32
        }
        
        private string _name;
        private KindOfSymbol _kind;
        private List<DimensionnedType> _types;
        private SymbolTable _linked_table;
        private int _offset;
        private bool _resolved;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        internal KindOfSymbol Kind
        {
            get
            {
                return _kind;
            }

            set
            {
                _kind = value;
            }
        }

        public List<DimensionnedType> Types
        {
            get
            {
                return _types;
            }

            set
            {
                _types = value;
            }
        }

        public SymbolTable LinkedTable
        {
            get
            {
                return _linked_table;
            }
            set
            {
                _linked_table = value;
            }
        }

        public bool Resolved
        {
            get
            {
                return _resolved;
            }
            set
            {
                _resolved = true;
            }
        }

        public int Offset
        {
            get
            {
                return _offset;
            }

            set
            {
                _offset = value;
            }
        }

        public SymbolEntry(string name, KindOfSymbol kind, DimensionnedType singleType, SymbolTable link) : this(name, kind, new List<DimensionnedType>() { singleType}, link)
        {

        }
        public SymbolEntry(string name, KindOfSymbol kind, List<DimensionnedType> types, SymbolTable link)
        {
            _name = name;
            _kind = kind;
            _types = types;
            _linked_table = link;
            _offset = 0;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(_name).Append(", ");
            sb.Append(_kind.ToString()).Append(", (");
            foreach(DimensionnedType dt in _types)
            {
                sb.Append(dt.Type.ToString());
                if (dt.ClassName != null)
                {
                    sb.Append(" ").Append(dt.ClassName);
                }
                foreach(int i in dt.Dimensions)
                {
                    sb.Append("[").Append(i).Append("]");
                }
                sb.Append(", ");
            }
            sb.Remove(sb.Length - 2,2);
            sb.Append(") ");
            if (_linked_table != null)
            {
                sb.Append(_linked_table.ToString());
            }
            else
            {
                sb.Append("none");
            }
            
            return sb.ToString();
        }
    }
}

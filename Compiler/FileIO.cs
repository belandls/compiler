﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : FileIO.cs
 * Description : Takes care of reading the file into memory and also takes care
 *               of structuring the raw data so that the analysers can easily read
 *               the RAW data and know where they are.
 * 
 * */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler
{
    public class FileIO
    {

        private int _current_line_number;
        private string _current_line;
        private int _current_pos_in_line;
        private Queue<string> _lines;
        private bool _eof;

        public string CurrentLine
        {
            get
            {
                //Only get the part that has not been analyzed yet
                return _current_line.Substring(_current_pos_in_line);
            }
        }
        public int CurrentPositionInLine
        {
            get
            {
                return _current_pos_in_line;
            }
        }
        public int CurrentLineNumber
        {
            get
            {
                return _current_line_number;
            }
        }
        public char NextCharacter
        {
            get
            {

                return _current_line.ElementAt(_current_pos_in_line);
            }
        }
        public bool EOF
        {
            get
            {
                return _eof;
            }
        }

        public FileIO()
        {
            _current_line_number = 0;
            _lines = new Queue<string>();
            _eof = false;
        }

        public void LoadFile(string filename)
        {
            //Load File into memory to be able to work on the file without keeping it opened
            using (var sr = new StreamReader(filename))
            {
                while (sr.Peek() >= 0)
                {
                    string temp = sr.ReadLine();
                    if (temp.Length > 0)
                    {
                        _lines.Enqueue(temp);
                    }
                    else
                    {
                        _lines.Enqueue(" "); //simply to count the line
                    }
                }
            }
            GetNextWorkingLine(); //Load the first line
        }

        //For use in unit testing
        public void LoadStrings(List<string> lines)
        {
            foreach (string line in lines)
            {
                _lines.Enqueue(line);
            }
            GetNextWorkingLine();
        }

        //Update the current position in line from the last found token. If the line if finished, load the next line
        public void UpdateCurrentLine(Token tok)
        {
            if (tok.Type == TokenType.T_WHITESPACE)
            {
                _current_pos_in_line += 1;
            }
            else if (tok.IsMultiline)
            {
                _current_pos_in_line += tok.EndPosition.PositionInLine;
            }
            else
            {
                _current_pos_in_line += tok.Value.Length;
            }
            if (_current_pos_in_line >= _current_line.Length)
            {
                GetNextWorkingLine();
            }
        }

        //Load the next working line if possible
        public void GetNextWorkingLine()
        {
            if (_lines.Count > 0)
            {
                _current_line = _lines.Dequeue();
                _current_line_number++;
                _current_pos_in_line = 0;
            }
            else
            {
                //If there is no more line to load, we flag the EOF
                _eof = true;
            }
        }

    }
}

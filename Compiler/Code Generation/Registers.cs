﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.Code_Generation
{
    public class Registers
    {
        private string _indice_register;
        private string _stack_offset_register;
        private string _stack_top_register;
        private List<string> _intermediate_registers;
        private int _current_intermediate_reg;
        
        public Registers()
        {
            _current_intermediate_reg = -1;
            _intermediate_registers = new List<string>() { "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "r10" };
            _indice_register = "r11";
            _stack_offset_register = "r12";
            _stack_top_register = "r14";
        }

        public string CurrentIntermediateRegister
        {
            get
            {
                return _intermediate_registers[_current_intermediate_reg];
            }
        }

        public string NextIntermediateRegister
        {
            get
            {
                return _intermediate_registers[_current_intermediate_reg + 1];
            }
        }

        public string PreviousIntermediateRegister
        {
            get
            {
                return _intermediate_registers[_current_intermediate_reg - 1];
            }
        }
        private bool IsAtBeginning
        {
            get
            {
                return _current_intermediate_reg == 0;
            }
        } 
        public string IndiceRegister
        {
            get
            {
                return _indice_register;
            }
        }

        public string StackOffsetRegister
        {
            get
            {
                return _stack_offset_register;
            }
        }

        public string StackTopRegister
        {
            get
            {
                return _stack_top_register;
            }
        }

        public void ReserveRegister()
        {
            _current_intermediate_reg++;
        }
        public void ReleaseRegister()
        {
            _current_intermediate_reg--;
        }
    }
}

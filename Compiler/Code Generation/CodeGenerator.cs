﻿using Compiler.SemanticAnalyser.SemanticActions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.Code_Generation
{
    class CodeGenerator
    {
        private StreamWriter _sw;
        private int _if_counter;
        private Stack<int> _current_if_counter;
        private int _for_counter;
        private Stack<int> _current_for_counter;
        Registers _reg;
        public CodeGenerator(string outName)
        {
            _sw = new StreamWriter(outName, false);
            _sw.AutoFlush = true;
            _if_counter = 0;
            _current_if_counter = new Stack<int>();
            _for_counter = 0;
            _current_for_counter = new Stack<int>();
            _reg = new Registers();
        }

        public void GenerateEntryPoint()
        {
            _sw.WriteLine("begin\tres 10000");
            _sw.WriteLine("\t\tentry");
            _sw.WriteLine(String.Format("\t\taddi {0},r0,begin",_reg.StackTopRegister));
        }

        public void GenerateVariableStack(SymbolTable table)
        {
            _sw.Write(String.Format("\t\taddi {0},{0},", _reg.StackTopRegister));
            _sw.WriteLine(table.Size);
        }

        public void GenerateExpression(Expression e, SymbolTable table)
        {
            Expression exp = (Expression)e.ExpressionStack.Pop().Value;
            RecursiveGeneration2(exp, table);
            _reg.ReleaseRegister();
            //RecursiveGeneration(exp, table,false,_reg.CurrentIntermediateRegister);
            //ClearReg("r3");
        }

        public void GenerateIndiceExpression(NestedId nid, SymbolTable table)
        {
            //ClearReg("r5");
            List<Expression> expToGenerate = new List<Expression>();
            expToGenerate.AddRange(nid.Initial.Dimensions);
            bool inter = false;
            inter = DumpIndice(expToGenerate, nid.Initial.ReferenceDimensions, inter, table);
            if (nid.Ids.Count > 0)
            {
                //SymbolTable classTable = 
                foreach (var id in nid.Ids)
                {
                    
                }
            }
            

            //_sw.WriteLine("\t\tandi r5,r5,0");
            //foreach(Expression e in expToGenerate)
            //{
            //    //Expression exp = (Expression)e.ExpressionStack.Pop().Value;
            //    inter = RecursiveGeneration(e, table, inter, "r5");
            //}

        }

        private void ClearReg(string reg)
        {
            _sw.WriteLine(String.Format("\t\tandi {0},{0},0", reg));
        }

        private bool DumpIndice(List<Expression> exp, List<int> dimRef, bool inter, SymbolTable table)
        {
            ClearReg(_reg.IndiceRegister);
            for (int i = 0; i < dimRef.Count; ++i)
            {
                if (i < dimRef.Count - 1)
                {
                    Expression test = new Expression();
                    test.AddExpressionElement(new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.EXPRESSION_INT, Expression.ExpressionStackElement.Cardinality.NA, exp[i]));
                    test.AddExpressionElement(new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.IMMEDIATE_INT, Expression.ExpressionStackElement.Cardinality.NA, dimRef[i]));
                    test.AddExpressionElement(new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.OPERATOR, Expression.ExpressionStackElement.Cardinality.BINARY, "*"));
                    RecursiveGeneration2(test, table);

                    //inter = RecursiveGeneration(test, table, inter, "r5");
                }
                else
                {
                    RecursiveGeneration2(exp[i], table);
                    //inter = RecursiveGeneration(exp[i], table, inter, "r5");
                }
                _reg.ReleaseRegister();
                _sw.WriteLine(String.Format("\t\tadd {0},{0},{1}", _reg.IndiceRegister, _reg.NextIntermediateRegister));
            }

            return inter;
        }

        private void RecursiveGeneration2(Expression e, SymbolTable table, int stackOffset = 0)
        {
            _sw.WriteLine("\t\t%reserving...");
            _reg.ReserveRegister();
            ClearReg(_reg.CurrentIntermediateRegister);
            if (e.ExpressionStack.Count == 1)
            {
                var se = e.ExpressionStack.Pop();
                if(se.Value is Expression)
                {
                    RecursiveGeneration2((Expression)se.Value, table);
                }
                else
                {
                    switch (se.Type)
                {
                    case Expression.ExpressionStackElement.ElementType.IMMEDIATE_INT:
                        _sw.WriteLine(String.Format("\t\taddi {0},{0},{1}", _reg.CurrentIntermediateRegister, se.Value));
                        break;
                    case Expression.ExpressionStackElement.ElementType.RETURN_INT:
                        _sw.WriteLine(String.Format("\t\tlw {1},4({0})", _reg.StackTopRegister, _reg.CurrentIntermediateRegister));
                        break;
                    case Expression.ExpressionStackElement.ElementType.VARIABLE_INT:
                        GenerateIndiceExpression((NestedId)se.Value, table);
                        int off = table.GetStackOffsetForVar((NestedId)se.Value);
                        int tableOff = table.GetCurrentTableOffset();
                        _sw.WriteLine(String.Format("\t\tmuli {0},{0},4", _reg.IndiceRegister));
                        _sw.WriteLine(String.Format("\t\tsubi {1},{2},{0}", table.Size + tableOff, _reg.StackOffsetRegister, _reg.StackTopRegister));
                        _sw.WriteLine(String.Format("\t\tadd {0},{0},{1}", _reg.IndiceRegister, _reg.StackOffsetRegister));
                        _sw.WriteLine(String.Format("\t\tlw {2},{0}({1})", off, _reg.IndiceRegister, _reg.CurrentIntermediateRegister));
                        //GenerateIndiceExpression((NestedId)se.Value, table);
                        //int off = table.GetStackOffsetForVar((NestedId)se.Value);
                        //_sw.WriteLine("\t\tmuli r5,r5,4");
                        //_sw.WriteLine(String.Format("\t\tsubi r6,r15,{0}", table.Size));
                        //_sw.WriteLine("\t\tadd r5,r5,r6");
                        //_sw.WriteLine(String.Format("\t\tlw {0},{1}(r5)", reg, off));
                        //ClearReg("r5");
                        //int i = 0;
                        break;
                }
                }
                
            }
            else if (e.ExpressionStack.Count == 2)
            {
                var oper = e.ExpressionStack.Pop();
                var operand1 = e.ExpressionStack.Pop();

                if (operand1.Value is Expression)
                {
                    RecursiveGeneration2((Expression)operand1.Value, table);
                    _reg.ReleaseRegister();
                    _sw.WriteLine(String.Format("\t\tadd {0},r0,{1}", _reg.CurrentIntermediateRegister, _reg.NextIntermediateRegister));
                }
                else
                {

                    switch (operand1.Type)
                    {
                        case Expression.ExpressionStackElement.ElementType.IMMEDIATE_INT:
                            _sw.WriteLine(String.Format("\t\taddi {0},r0,{1}", _reg.CurrentIntermediateRegister, operand1.Value));
                            break;
                        case Expression.ExpressionStackElement.ElementType.RETURN_INT:
                            _sw.WriteLine(String.Format("\t\tlw {1},4({0})", _reg.StackTopRegister, _reg.CurrentIntermediateRegister));
                            break;
                        case Expression.ExpressionStackElement.ElementType.VARIABLE_INT:
                            //_sw.WriteLine("\t\t%reserving...");
                            //_reg.ReserveRegister();
                            GenerateIndiceExpression((NestedId)operand1.Value, table);
                            int off = table.GetStackOffsetForVar((NestedId)operand1.Value);
                            int tableOff = table.GetCurrentTableOffset();
                            _sw.WriteLine(String.Format("\t\tmuli {0},{0},4", _reg.IndiceRegister));
                            _sw.WriteLine(String.Format("\t\tsubi {1},{2},{0}", table.Size  + tableOff, _reg.StackOffsetRegister, _reg.StackTopRegister));
                            _sw.WriteLine(String.Format("\t\tadd {0},{0},{1}", _reg.IndiceRegister, _reg.StackOffsetRegister));
                            _sw.WriteLine(String.Format("\t\tlw {2},{0}({1})", off, _reg.IndiceRegister, _reg.CurrentIntermediateRegister));
                            //_reg.ReleaseRegister();
                            break;
                    }
                }
                switch ((string)oper.Value)
                {
                    case "+":   
                        break;
                    case "-":
                        _sw.WriteLine(String.Format("\t\tmuli {0},{0},-1",  _reg.CurrentIntermediateRegister));
                        break;
                    case "not":
                        _sw.WriteLine(String.Format("\t\tnot {0},{0}", _reg.CurrentIntermediateRegister));
                        break;
                }
            }
            else
            {
                bool assigning = false;
                var oper = e.ExpressionStack.Pop();
                var operand1 = e.ExpressionStack.Pop();
                var operand2 = e.ExpressionStack.Pop();

                if (operand2.Value is Expression)
                {
                    RecursiveGeneration2((Expression)operand2.Value, table);
                    _reg.ReleaseRegister();
                    _sw.WriteLine(String.Format("\t\tadd {0},r0,{1}", _reg.CurrentIntermediateRegister, _reg.NextIntermediateRegister));
                }
                else
                {
                    if ((string)oper.Value == "=")
                    {
                        assigning = true;
                    }
                    else
                    {
                        switch (operand2.Type)
                        {
                            case Expression.ExpressionStackElement.ElementType.IMMEDIATE_INT:
                                _sw.WriteLine(String.Format("\t\taddi {0},r0,{1}", _reg.CurrentIntermediateRegister, operand2.Value));
                                break;
                            case Expression.ExpressionStackElement.ElementType.RETURN_INT:
                                _sw.WriteLine(String.Format("\t\tlw {1},4({0})", _reg.StackTopRegister, _reg.CurrentIntermediateRegister));
                                break;
                            case Expression.ExpressionStackElement.ElementType.VARIABLE_INT:
                                //_sw.WriteLine("\t\t%reserving...");
                                //_reg.ReserveRegister();
                                GenerateIndiceExpression((NestedId)operand2.Value, table);
                                int off = table.GetStackOffsetForVar((NestedId)operand2.Value);
                                int tableOff = table.GetCurrentTableOffset();
                                _sw.WriteLine(String.Format("\t\tmuli {0},{0},4", _reg.IndiceRegister));
                                _sw.WriteLine(String.Format("\t\tsubi {1},{2},{0}", table.Size + tableOff, _reg.StackOffsetRegister, _reg.StackTopRegister));
                                _sw.WriteLine(String.Format("\t\tadd {0},{0},{1}", _reg.IndiceRegister, _reg.StackOffsetRegister));
                                _sw.WriteLine(String.Format("\t\tlw {2},{0}({1})", off, _reg.IndiceRegister, _reg.CurrentIntermediateRegister));
                                //_reg.ReleaseRegister();
                                break;
                        }
                    }
                }
                bool secondOperandExpression = false;
                if (operand1.Value is Expression)
                {
                    RecursiveGeneration2((Expression)operand1.Value, table);
                    _reg.ReleaseRegister();
                    secondOperandExpression = true;
                }
                else
                {

                }
                if (assigning)
                {
                    if (!secondOperandExpression)
                    {
                        switch (operand1.Type)
                        {
                            case Expression.ExpressionStackElement.ElementType.IMMEDIATE_INT:
                                _sw.WriteLine(String.Format("\t\taddi {0},{0},{1}", _reg.CurrentIntermediateRegister, operand1.Value));
                                break;
                            case Expression.ExpressionStackElement.ElementType.RETURN_INT:
                                _sw.WriteLine(String.Format("\t\tlw {1},4({0})", _reg.StackTopRegister, _reg.CurrentIntermediateRegister));
                                break;
                            case Expression.ExpressionStackElement.ElementType.VARIABLE_INT:
                                GenerateIndiceExpression((NestedId)operand1.Value, table);
                                int off2 = table.GetStackOffsetForVar((NestedId)operand1.Value);
                                int tableOff2 = table.GetCurrentTableOffset();
                                _sw.WriteLine(String.Format("\t\tmuli {0},{0},4", _reg.IndiceRegister));
                                _sw.WriteLine(String.Format("\t\tsubi {1},{2},{0}", table.Size + tableOff2, _reg.StackOffsetRegister, _reg.StackTopRegister));
                                _sw.WriteLine(String.Format("\t\tadd {0},{0},{1}", _reg.IndiceRegister, _reg.StackOffsetRegister));
                                _sw.WriteLine(String.Format("\t\tlw {2},{0}({1})", off2, _reg.IndiceRegister, _reg.CurrentIntermediateRegister));
                                //GenerateIndiceExpression((NestedId)se.Value, table);
                                //int off = table.GetStackOffsetForVar((NestedId)se.Value);
                                //_sw.WriteLine("\t\tmuli r5,r5,4");
                                //_sw.WriteLine(String.Format("\t\tsubi r6,r15,{0}", table.Size));
                                //_sw.WriteLine("\t\tadd r5,r5,r6");
                                //_sw.WriteLine(String.Format("\t\tlw {0},{1}(r5)", reg, off));
                                //ClearReg("r5");
                                //int i = 0;
                                break;
                        }
                    }
                    //_sw.WriteLine("\t\t%reserving...");
                    //_reg.ReserveRegister();
                    GenerateIndiceExpression((NestedId)operand2.Value, table);
                    int off = table.GetStackOffsetForVar((NestedId)operand2.Value);
                    int tableOff = table.GetCurrentTableOffset();
                    _sw.WriteLine(String.Format("\t\tmuli {0},{0},4", _reg.IndiceRegister));
                    _sw.WriteLine(String.Format("\t\tsubi {1},{2},{0}", table.Size + tableOff, _reg.StackOffsetRegister, _reg.StackTopRegister));
                    _sw.WriteLine(String.Format("\t\tadd {0},{0},{1}", _reg.IndiceRegister, _reg.StackOffsetRegister));
                    if (secondOperandExpression)
                    {
                        _sw.WriteLine(String.Format("\t\tsw {0}({1}),{2}", off, _reg.IndiceRegister, _reg.NextIntermediateRegister));
                    }
                    else
                    {
                        _sw.WriteLine(String.Format("\t\tsw {0}({1}),{2}", off, _reg.IndiceRegister, _reg.CurrentIntermediateRegister));
                    }

                    //_reg.ReleaseRegister();
                }
                else
                {
                    if (operand1.Type == Expression.ExpressionStackElement.ElementType.VARIABLE_INT)
                    {
                        _sw.WriteLine("\t\t%reserving...");
                        _reg.ReserveRegister();
                        GenerateIndiceExpression((NestedId)operand1.Value, table);
                        int off = table.GetStackOffsetForVar((NestedId)operand1.Value);
                        int tableOff = table.GetCurrentTableOffset();
                        _sw.WriteLine(String.Format("\t\tmuli {0},{0},4", _reg.IndiceRegister));
                        _sw.WriteLine(String.Format("\t\tsubi {1},{2},{0}", table.Size + tableOff, _reg.StackOffsetRegister, _reg.StackTopRegister));
                        _sw.WriteLine(String.Format("\t\tadd {0},{0},{1}", _reg.IndiceRegister, _reg.StackOffsetRegister));
                        _sw.WriteLine(String.Format("\t\tlw {2},{0}({1})", off, _reg.IndiceRegister, _reg.CurrentIntermediateRegister));
                        _reg.ReleaseRegister();
                        secondOperandExpression = true;
                    }else if(operand1.Type == Expression.ExpressionStackElement.ElementType.RETURN_INT)
                    {
                        _reg.ReserveRegister();
                        _sw.WriteLine(String.Format("\t\tlw {1},4({0})", _reg.StackTopRegister, _reg.CurrentIntermediateRegister));
                        _reg.ReleaseRegister();
                        secondOperandExpression = true;
                    }
                    string op = null;
                    switch ((string)oper.Value)
                    {
                        case "+":
                            if (secondOperandExpression)
                            {
                                op = "add";
                            }
                            else
                            {
                                op = "addi";
                            }
                            break;
                        case "*":
                            if (secondOperandExpression)
                            {
                                op = "mul";
                            }
                            else
                            {
                                op = "muli";
                            }
                            break;
                        case "-":
                            if (secondOperandExpression)
                            {
                                op = "sub";
                            }
                            else
                            {
                                op = "subi";
                            }
                            break;
                        case "and":
                            if (secondOperandExpression)
                            {
                                op = "and";
                            }
                            else
                            {
                                op = "andi";
                            }
                            break;
                        case "or":
                            if (secondOperandExpression)
                            {
                                op = "or";
                            }
                            else
                            {
                                op = "ori";
                            }
                            break;
                        case "==":
                            if (secondOperandExpression)
                            {
                                op = "ceq";
                            }
                            else
                            {
                                op = "ceqi";
                            }
                            break;
                        case "<>":
                            if (secondOperandExpression)
                            {
                                op = "cneq";
                            }
                            else
                            {
                                op = "cneqi";
                            }
                            break;
                        case ">":
                            if (secondOperandExpression)
                            {
                                op = "cgt";
                            }
                            else
                            {
                                op = "cgti";
                            }
                            break;
                        case ">=":
                            if (secondOperandExpression)
                            {
                                op = "cge";
                            }
                            else
                            {
                                op = "cgei";
                            }
                            break;
                        case "<":
                            if (secondOperandExpression)
                            {
                                op = "clt";
                            }
                            else
                            {
                                op = "clti";
                            }
                            break;
                        case "<=":
                            if (secondOperandExpression)
                            {
                                op = "cle";
                            }
                            else
                            {
                                op = "clei";
                            }
                            break;
                            //case "==":
                            //    _sw.WriteLine(String.Format("\t\tceq {0},r1,{1}", dest, src));
                            //    break;
                    }
                    if (secondOperandExpression)
                    {
                        _sw.WriteLine(String.Format("\t\t{0} {1},{1},{2}", op, _reg.CurrentIntermediateRegister, _reg.NextIntermediateRegister));

                    }
                    else
                    {
                        _sw.WriteLine(String.Format("\t\t{0} {1},{1},{2}", op, _reg.CurrentIntermediateRegister, operand1.Value));
                    }
                }

            }
        }

        public void GenerateVariableStore(NestedId nid, SymbolTable table, string src)
        {
            GenerateIndiceExpression(nid, table);
            int off = table.GetStackOffsetForVar(nid);
            int tableOff = table.GetCurrentTableOffset();
            _sw.WriteLine(String.Format("\t\tmuli {0},{0},4", _reg.IndiceRegister));
            _sw.WriteLine(String.Format("\t\tsubi {1},{2},{0}", table.Size /*+ tableOff*/, _reg.StackOffsetRegister, _reg.StackTopRegister));
            _sw.WriteLine(String.Format("\t\tadd {0},{0},{1}", _reg.IndiceRegister, _reg.StackOffsetRegister));
            _sw.WriteLine(String.Format("\t\tsw {0}({1}),{2}", off, _reg.IndiceRegister, src));
        }
        //private bool RecursiveGeneration(Expression e, SymbolTable table, bool intermediate, string reg)
        //{
        //    if (e.ExpressionStack.Count == 1)
        //    {
        //        var se = e.ExpressionStack.Pop();
        //        switch (se.Type)
        //        {
        //            case Expression.ExpressionStackElement.ElementType.IMMEDIATE_INT:
        //                _sw.WriteLine(String.Format("\t\taddi {0},{0},{1}", reg, se.Value));
        //                break;
        //            case Expression.ExpressionStackElement.ElementType.VARIABLE_INT:
        //                GenerateIndiceExpression((NestedId)se.Value, table);
        //                int off = table.GetStackOffsetForVar((NestedId)se.Value);
        //                _sw.WriteLine("\t\tmuli r5,r5,4");
        //                _sw.WriteLine(String.Format("\t\tsubi r6,r15,{0}", table.Size));
        //                _sw.WriteLine("\t\tadd r5,r5,r6");
        //                _sw.WriteLine(String.Format("\t\tlw {0},{1}(r5)", reg, off));
        //                //ClearReg("r5");
        //                //int i = 0;
        //                break;
        //        }
        //    }
        //    else
        //    {
        //        var oper = e.ExpressionStack.Pop();
        //        if (oper.Card == Expression.ExpressionStackElement.Cardinality.BINARY)
        //        {
        //            if ((string)oper.Value == "=")
        //            {
        //                var operand1 = e.ExpressionStack.Pop();
        //                var operand2 = e.ExpressionStack.Pop();
        //                Expression expTemp = null;
        //                if (operand1.Value is Expression)
        //                {
        //                    expTemp = (Expression)operand1.Value;
        //                }
        //                else
        //                {
        //                    expTemp = new Expression();
        //                    expTemp.AddExpressionElement(operand1);
        //                }
        //                intermediate = RecursiveGeneration(expTemp, table, intermediate, "r3");
        //                GenerateIndiceExpression((NestedId)operand2.Value, table);
        //                int off = table.GetStackOffsetForVar((NestedId)operand2.Value);
        //                _sw.WriteLine("\t\tmuli r5,r5,4");
        //                _sw.WriteLine(String.Format("\t\tsubi r6,r15,{0}", table.Size));
        //                _sw.WriteLine("\t\tadd r5,r5,r6");
        //                _sw.WriteLine(String.Format("\t\tsw {0}(r5),r3", off));
        //                //ClearReg("r5");
        //            }
        //            else
        //            {
        //                var operand1 = e.ExpressionStack.Pop();
        //                var operand2 = e.ExpressionStack.Pop();
        //                int loadedReg = 0;

        //                if (operand2.Value is Expression)
        //                {
        //                    if (operand1.Value is Expression)
        //                    {
        //                        _reg.ReserveRegister();
        //                    }

        //                    intermediate = RecursiveGeneration((Expression)operand2.Value, table, intermediate, reg);

        //                }
        //                else
        //                {
        //                    string lr;
        //                    if (operand1.Value is Expression)
        //                    {
        //                        lr = "r1";
        //                    }
        //                    else
        //                    {
        //                        lr = "r2";
        //                    }
        //                    switch (operand2.Type)
        //                    {
        //                        case Expression.ExpressionStackElement.ElementType.IMMEDIATE_INT:
        //                            _sw.WriteLine(String.Format("\t\taddi {0},r0,{1}", lr, operand2.Value));
        //                            break;
        //                        case Expression.ExpressionStackElement.ElementType.VARIABLE_INT:
        //                            GenerateIndiceExpression((NestedId)operand2.Value, table);
        //                            int off = table.GetStackOffsetForVar((NestedId)operand2.Value);
        //                            _sw.WriteLine("\t\tmuli r5,r5,4");
        //                            _sw.WriteLine(String.Format("\t\tsubi r6,r15,{0}", table.Size));
        //                            _sw.WriteLine("\t\tadd r5,r5,r6");
        //                            _sw.WriteLine(String.Format("\t\tlw {0},{1}(r5)", lr, off));
        //                            //ClearReg("r5");
        //                            break;
        //                    }
        //                    loadedReg++;
        //                }

        //                if (operand1.Value is Expression)
        //                {
        //                    intermediate = RecursiveGeneration((Expression)operand1.Value, table, intermediate, reg);
        //                }
        //                else
        //                {
        //                    switch (operand1.Type)
        //                    {
        //                        case Expression.ExpressionStackElement.ElementType.IMMEDIATE_INT:
        //                            _sw.WriteLine(String.Format("\t\taddi r1,r0,{0}", operand1.Value));
        //                            break;
        //                        case Expression.ExpressionStackElement.ElementType.VARIABLE_INT:
        //                            GenerateIndiceExpression((NestedId)operand1.Value, table);
        //                            int off = table.GetStackOffsetForVar((NestedId)operand1.Value);
        //                            _sw.WriteLine("\t\tmuli r5,r5,4");
        //                            _sw.WriteLine(String.Format("\t\tsubi r6,r15,{0}", table.Size));
        //                            _sw.WriteLine("\t\tadd r5,r5,r6");
        //                            _sw.WriteLine(String.Format("\t\tlw {0},{1}(r5)", "r1", off));
        //                            //ClearReg("r5");
        //                            break;
        //                    }
        //                    loadedReg++;
        //                }


        //                string dest;
        //                string src;

        //                if (loadedReg == 2 && intermediate)
        //                {
        //                    dest = "r1";
        //                }
        //                else
        //                {
        //                    dest = reg;
        //                }

        //                if (loadedReg == 2)
        //                {
        //                    src = "r2";
        //                }
        //                else
        //                {
        //                    src = reg;
        //                }
        //                switch ((string)oper.Value)
        //                {
        //                    case "=":
        //                        GenerateIndiceExpression((NestedId)operand2.Value, table);
        //                        int off = table.GetStackOffsetForVar((NestedId)operand2.Value);
        //                        _sw.WriteLine("\t\tmuli r5,r5,4");
        //                        //_sw.WriteLine("\t\tandi r6,r6,0");
        //                        _sw.WriteLine(String.Format("\t\tsubi r6,r15,{0}", table.Size));
        //                        _sw.WriteLine("\t\tadd r5,r5,r6");
        //                        _sw.WriteLine(String.Format("\t\tsw {0}(r5),r3", off));
        //                        //_sw.WriteLine(String.Format("equals"));
        //                        //_sw.WriteLine(String.Format("\t\tsw {0}r3,r1", "r2"));
        //                        break;
        //                    case "+":
        //                        _sw.WriteLine(String.Format("\t\tadd {0},r1,{1}", dest, src));
        //                        break;
        //                    case "*":
        //                        _sw.WriteLine(String.Format("\t\tmul {0},r1,{1}", dest, src));
        //                        break;
        //                    case "==":
        //                        _sw.WriteLine(String.Format("\t\tceq {0},r1,{1}", dest, src));
        //                        break;
        //                }
        //            }
        //        }
        //    }

        //    return true;
        //    //    var oper = e.ExpressionStack.Pop();
        //    //    if (oper.Card == Expression.ExpressionStackElement.Cardinality.BINARY)
        //    //    {
        //    //        var operand1 = e.ExpressionStack.Pop();
        //    //        var operand2 = e.ExpressionStack.Pop();

        //    //        string intermediateRegister = "r3";

        //    //        if (operand1.Value is Expression)
        //    //        {
        //    //            RecursiveGeneration((Expression)operand1.Value, table);
        //    //        }
        //    //        else
        //    //        {
        //    //            switch (operand1.Type)
        //    //            {
        //    //                case Expression.ExpressionStackElement.ElementType.IMMEDIATE_INT:
        //    //                    _sw.WriteLine(String.Format("\t\taddi r1,r0,{0}", operand1.Value));
        //    //                    break;
        //    //            }
        //    //            if (operand2.Value is Expression)
        //    //            {
        //    //                int i = 0;
        //    //            }
        //    //            else
        //    //            {

        //    //            }
        //    //        }
        //    //        if (operand2.Value is Expression)
        //    //        {
        //    //            RecursiveGeneration((Expression)operand1.Value, table);
        //    //        }
        //    //        else
        //    //        {
        //    //            switch (operand2.Type)
        //    //            {
        //    //                case Expression.ExpressionStackElement.ElementType.IMMEDIATE_INT:

        //    //                    //_sw.WriteLine(String.Format("\t\taddi {0}(r15),{1}", , operand2.Value));
        //    //                    break;
        //    //            }
        //    //            if (operand1.Value is Expression)
        //    //            {
        //    //                //_sw.WriteLine(String.Format("\t\tadd r1,r0,{0}", "r3"));
        //    //            }
        //    //            else
        //    //            {

        //    //            }
        //    //        }
        //    //        switch ((string)oper.Value)
        //    //        {
        //    //            case "=":
        //    //                _sw.WriteLine(String.Format("\t\tsw {0}r3,r1", "r2"));
        //    //                break;
        //    //            case "+":
        //    //                _sw.WriteLine(String.Format("\t\tadd r3,r1,{0}", "r2"));
        //    //                break;
        //    //        }
        //    //    }
        //}

        public void GenerateIfBeginning(Expression e, SymbolTable table)
        {
            _current_if_counter.Push(++_if_counter);
            Expression exp = (Expression)e.ExpressionStack.Pop().Value;
            RecursiveGeneration2(exp, table);
            _reg.ReleaseRegister();
            _sw.WriteLine(String.Format("\t\tbz {0},else{1}", _reg.NextIntermediateRegister, _if_counter));
        }
        public void GenerateElse()
        {
            int iftemp = _current_if_counter.Pop();
            _sw.WriteLine("\t\tj endif" + iftemp);
            _sw.WriteLine(String.Format("else{0}\tnop", iftemp));
            _current_if_counter.Push(iftemp);
        }
        public void GenerateEndIf()
        {
            int iftemp = _current_if_counter.Pop();
            _sw.WriteLine(String.Format("endif{0}\tnop", iftemp));
        }

        public void GenerateEndOfProgram()
        {
            _sw.WriteLine("\t\thlt");
            GenerateGetInt();
            GeneratePutInt();
        }

        public void GenerateForBeginning(Expression init, Expression cond, SymbolTable table)
        {
            _current_for_counter.Push(++_for_counter);
            GenerateExpression(init, table);
            _sw.WriteLine(String.Format("for_cond{0}\tnop", _for_counter));
            Expression exp = (Expression)cond.ExpressionStack.Pop().Value;
            RecursiveGeneration2(exp, table);
            _reg.ReleaseRegister();
            _sw.WriteLine(String.Format("\t\tbz {0},for_end{1}", _reg.NextIntermediateRegister, _for_counter));
        }

        public void GenerateForEnd(Expression incr, SymbolTable table)
        {
            int fortemp = _current_for_counter.Pop();
            GenerateExpression(incr, table);
            _sw.WriteLine("\t\tj for_cond" + fortemp);
            _sw.WriteLine(String.Format("for_end{0}\tnop", fortemp));
            RemoveFromStack(table);
        }

        public void GeneratePut(Expression output, SymbolTable table)
        {
            RecursiveGeneration2(output, table);
            _reg.ReleaseRegister();
            StackReturnAddr();
            _sw.WriteLine("\t\tjl r15,putint");
            RestoreReturnAddr();
        }

        public void GenerateRead(NestedId input, SymbolTable table)
        {
            StackReturnAddr();
            _sw.WriteLine("\t\tjl r15,getint");
            RestoreReturnAddr();
            GenerateIndiceExpression(input, table);
            int off = table.GetStackOffsetForVar(input);
            int tableOff = table.GetCurrentTableOffset();
            _sw.WriteLine(String.Format("\t\tmuli {0},{0},4", _reg.IndiceRegister));
            _sw.WriteLine(String.Format("\t\tsubi {1},{2},{0}", table.Size + tableOff, _reg.StackOffsetRegister, _reg.StackTopRegister));
            _sw.WriteLine(String.Format("\t\tadd {0},{0},{1}", _reg.IndiceRegister, _reg.StackOffsetRegister));
            _sw.WriteLine(String.Format("\t\tsw {0}({1}),{2}", off, _reg.IndiceRegister, "r1"));
        }

        public void RemoveFromStack(SymbolTable table)
        {
            _sw.WriteLine(String.Format("\t\tsubi {0},{0},{1}", _reg.StackTopRegister, table.Size));
        }

        public void GenerateFunctionCall(ParameterizedNestedId pnid, SymbolTable table)
        {
            //Create stack (find table first)
            SymbolTable fnTable = table.LookupNestedFunctionTable(pnid.Nid);

            if (fnTable == null)
                return;

            List<SymbolEntry> fnPar = fnTable.Entries.Where(se => se.Kind == SymbolEntry.KindOfSymbol.PARAMETER).ToList();
            fnTable.GenerateStackOffsets();
            fnPar.ForEach(i => i.Offset += table.Size + 4);
            table.Entries.AddRange(fnPar);
            //copie data in stack
            for(int i = 0; i < fnPar.Count; ++i)
            {
                //Expression tempAssExp = new Expression();
                //NestedId temp = new NestedId(new SemanticAnalyser.DimensionnedId(fnPar[i].Name, null), null);
                //temp.Initial.ReferenceDimensions = fnPar[i].Types[0].Dimensions;
                //tempAssExp.AddExpressionElement(new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.VARIABLE_INT, Expression.ExpressionStackElement.Cardinality.NA, temp));
                //tempAssExp.AddExpressionElement(new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.EXPRESSION_INT, Expression.ExpressionStackElement.Cardinality.BINARY, pnid.Params[i]));
                //tempAssExp.AddExpressionElement(new Expression.ExpressionStackElement(Expression.ExpressionStackElement.ElementType.OPERATOR, Expression.ExpressionStackElement.Cardinality.BINARY, "="));
                NestedId temp = new NestedId(new SemanticAnalyser.DimensionnedId(fnPar[i].Name, null), null);
                temp.Initial.ReferenceDimensions = fnPar[i].Types[0].Dimensions;
                RecursiveGeneration2(pnid.Params[i], table);
                GenerateVariableStore(temp, table, _reg.CurrentIntermediateRegister);
                _reg.ReleaseRegister();
            }
            //GenerateVariableStack(fnTable);
            fnPar.ForEach(i => i.Offset -= table.Size - 4);
            table.Entries.RemoveRange(table.Entries.Count - fnPar.Count, fnPar.Count);
            //jumpandlink

            StackReturnAddr();
            _sw.WriteLine("\t\tjl r15," + fnTable.Name + "_start");
            RemoveFromStack(fnTable);
            RestoreReturnAddr();
            
        }

        public void StackReturnAddr()
        {
            _sw.WriteLine("\t\tsw 0(r14),r15");
            _sw.WriteLine("\t\taddi r14,r14,4");
        }
        public void RestoreReturnAddr()
        {
            _sw.WriteLine("\t\tsubi r14,r14,4");
            _sw.WriteLine("\t\tlw r15,0(r14)");
        }

        public void GenerateBegFunctionDef(string idName, SymbolTable table)
        {
            table.GenerateStackOffsets();
            _sw.WriteLine(String.Format("{0}_start\t nop", idName));
        }

        public void GenerateEndFunctionDef(SymbolTable table)
        {
            _sw.WriteLine(String.Format("{0}_end\t nop", table.Name));
            _sw.WriteLine("\t\t jr r15");
        }

        public void GenerateFunctionReturn(Expression exp, SymbolTable table)
        {
            RecursiveGeneration2(exp, table);
            _sw.WriteLine(String.Format("\t\tandi  {0},{0},0", _reg.IndiceRegister));
            _sw.WriteLine(String.Format("\t\tsubi {1},{2},{0}", table.Size, _reg.StackOffsetRegister, _reg.StackTopRegister));
            _sw.WriteLine(String.Format("\t\tadd {0},{0},{1}", _reg.IndiceRegister, _reg.StackOffsetRegister));
            _sw.WriteLine(String.Format("\t\tsw {0}({1}),{2}", 0, _reg.IndiceRegister, _reg.CurrentIntermediateRegister));
            _reg.ReleaseRegister();
            _sw.WriteLine(String.Format("\t\tj {0}_end",table.Name));
        }

        public void GenerateGetInt()
        {
            _sw.WriteLine(@"
getint	align
	add	r1,r0,r0		% Initialize input register (r1 = 0)
	add	r2,r0,r0		% Initialize buffer's index i
	add	r4,r0,r0		% Initialize sign flag
getint1	getc	r1			% Input ch
	ceqi	r3,r1,43		% ch = '+' ?
	bnz	r3,getint1		% Branch if true (ch = '+')
	ceqi	r3,r1,45		% ch = '-' ?
	bz	r3,getint2		% Branch if false (ch != '-')
	addi	r4,r0,1			% Set sign flag to true
	j	getint1			% Loop to get next ch
getint2	clti	r3,r1,48		% ch < '0' ?
	bnz	r3,getint3		% Branch if true (ch < '0')
	cgti	r3,r1,57		% ch > '9' ?
	bnz	r3,getint3		% Branch if true (ch > '9')
	sb	getint9(r2),r1		% Store ch in buffer
	addi	r2,r2,1			% i++
	j	getint1			% Loop if not finished
getint3	sb	getint9(r2),r0		% Store end of string in buffer (ch = '\0')
	add	r2,r0,r0		% i = 0
	add	r1,r0,r0		% N = 0
	add	r3,r0,r0		% Initialize r3
getint4	lb	r3,getint9(r2)		% Load ch from buffer
	bz	r3,getint5		% Branch if end of string (ch = '\0')
	subi	r3,r3,48		% Convert to number (M)
	muli	r1,r1,10		% N *= 10
	add	r1,r1,r3		% N += M
	addi	r2,r2,1			% i++
	j	getint4			% Loop if not finished
getint5	bz	r4,getint6		% Branch if sign flag false
	sub	r1,r0,r1		% N = -N
getint6	jr	r15			% Return to the caller
getint9	res	12			% Local buffer (12 bytes)
	align");
        }
        public void GeneratePutInt()
        {
            _sw.WriteLine(@"
putint	align
	add	r2,r0,r0		% Initialize buffer's index i
	cge	r3,r1,r0		% True if N >= 0
	bnz	r3,putint1		% Branch if True (N >= 0)
	sub	r1,r0,r1		% N = -N
putint1	modi	r4,r1,10		% Rightmost digit
	addi	r4,r4,48		% Convert to ch
	divi	r1,r1,10		% Truncate rightmost digit
	sb	putint9(r2),r4		% Store ch in buffer
	addi	r2,r2,1			% i++
	bnz	r1,putint1		% Loop if not finished
	bnz	r3,putint2		% Branch if True (N >= 0)
	addi	r3,r0,45
	sb	putint9(r2),r3		% Store '-' in buffer
	addi	r2,r2,1			% i++
	add	r1,r0,r0		% Initialize output register (r1 = 0)
putint2	subi	r2,r2,1			% i--
	lb	r1,putint9(r2)		% Load ch from buffer
	putc	r1			% Output ch
	bnz	r2,putint2		% Loop if not finished
	jr	r15			% return to the caller
putint9	res	12			% loacl buffer (12 bytes)
	align");
        }
    }
}

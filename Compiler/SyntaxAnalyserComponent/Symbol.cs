﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : Symbol.cs
 * Description : Represents a terminal or a non-terminal in a production
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SyntaxAnalyserComponent
{
    public class Symbol
    {
        private bool _in_derivation;
        private bool _is_terminal;
        private string _value;
        private TokenType _associated_token_type;   //In the case it is a terminal, a TokenType is associated to ease up the comparaison with the input token

        public bool IsTerminal
        {
            get
            {
                return _is_terminal;
            }
        }

        public string Value
        {
            get
            {
                return _value;
            }
        }

        public TokenType AssociatedTokenType
        {
            get
            {
                return _associated_token_type;
            }
        }

        public bool InDerivation
        {
            get
            {
                return _in_derivation;
            }
        }

        public Symbol(string value)
            : this(value, false, TokenType.T_UNDEFINED, true)
        {

        }

        public Symbol(string value, bool inDerivation)
            : this(value, false, TokenType.T_UNDEFINED, inDerivation)
        {

        }

        public Symbol(string value, bool isTerminal, TokenType associatedTokenType) : this(value,isTerminal,associatedTokenType,true)
        {
        }

        public Symbol(string value, bool isTerminal, TokenType associatedTokenType, bool inDerivation)
        {
            _is_terminal = isTerminal;
            _value = value;
            _associated_token_type = associatedTokenType;
            _in_derivation = inDerivation;
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return _value;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            else if (this == obj)
            {
                return true;
            }
            else
            {
                Symbol s = obj as Symbol;
                return _value == s._value && _associated_token_type == s._associated_token_type && _is_terminal == s._is_terminal;
            }
        }

        //Checks if a symbol is equivalent to a token.
        public bool Equals(Token t)
        {
            if (!_is_terminal)
            {
                return false;
            }
            else
            {
                if (_associated_token_type == TokenType.T_UNDEFINED)
                {
                    return _value == t.Value;
                }
                //else if (_associated_token_type == TokenType.T_NUM_FLOAT)
                //{
                //    return t.Type == TokenType.T_NUM_FLOAT || t.Type == TokenType.T_NUM_INTEGER;
                //}
                else
                {
                    return _associated_token_type == t.Type;
                }
                
            }
        }

    }
}

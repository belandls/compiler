﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : Production.cs
 * Description : Represents a production of the form LHS -> RHS
 * 
 * */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SyntaxAnalyserComponent
{
    public class Production
    {
        //For error checking, dummy error productions are added in the parsing table
        public enum ProductionType
        {
            VALID,
            ERR_POP,
            ERR_SCAN
        }

        private Symbol _lhs;            //LHS of the production. Empty in dummy prod.
        private List<Symbol> _rhs;      //RHS of the production. Empty in dummy prod.
        private ProductionType _type;   //Type of the production.

        public Symbol LHS
        {
            get
            {
                return _lhs;
            }
        }

        public List<Symbol> RHS
        {
            get
            {
                return _rhs;
            }
        }

        public ProductionType Type
        {
            get
            {
                return _type;
            }
        }

        public bool IsEpsilon
        {
            get
            {
                return _rhs.Count >= 1 && ((Symbol)(_rhs[0])).Value == "eps";
            }
        }

        public Production(ProductionType pt)
        {
            _lhs = null;
            _rhs = null;
            _type = pt;
        }

        public Production(string lhs)
        {
            _lhs = new Symbol(lhs);
            _rhs = new List<Symbol>();
            _type = ProductionType.VALID;
        }

        public override int GetHashCode()
        {
            return _lhs.GetHashCode();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(LHS).Append(" -> ");
            foreach (Symbol s in RHS)
            {
                sb.Append(s).Append(" ");
            }
            return sb.ToString();
        }

        //Allows for chain adding. Used in the grammar generation script 
        public Production AddRHS(string value)
        {
            _rhs.Add(new Symbol(value));
            return this;
        }

        //Allows for chain adding. Used in the grammar generation script 
        public Production AddRHS(Symbol s)
        {
            _rhs.Add(s);
            return this;
        }
    }
}

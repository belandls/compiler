﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : SyntaxError.cs
 * Description : Represents a syntax error. Handles he error message generation based on the root symbol associated to it.
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SyntaxAnalyserComponent
{
    public class SyntaxError
    {
        private Token _source_token;       //Token where the error was detected (for position)
        private Symbol _derivation_root;   //The closest root of the current derivation when the error occured

        public SyntaxError(Token source)
            : this(source, null)
        {

        }

        public SyntaxError(Token source, Symbol derivationRoot)
        {
            _source_token = source;
            _derivation_root = derivationRoot;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("Syntax error at Line : {0}, Col. : {1} ('{2}'); ", _source_token.Position.LineNumber, _source_token.Position.PositionInLine, _source_token.Value);

            if (_derivation_root != null)
            {
                //Get the correct message based on the associated root
                switch (_derivation_root.Value)
                {
                    case "AS":
                        sb.Append("The array size is not valid");
                        break;
                    case "TI":
                        sb.Append("Invalid variable declaration or function header");
                        break;
                    case "SS":
                        sb.Append("Invalid statement structure");
                        break;
                    case "AEP":
                        sb.Append("Invalid arithmetic expression");
                        break;
                    case "RE":
                        sb.Append("Invalid Relational expression");
                        break;
                    case "TR":
                        sb.Append("Invalid Term");
                        break;
                    case "EP":
                    case "E":
                        sb.Append("Invalid Expression");
                        break;
                    case "FH":
                        sb.Append("Invalid function header");
                        break;
                    case "AGNS":
                        sb.Append("Assignment statement was expected");
                        break;
                    case "F":
                        sb.Append("Invalid Factor component");
                        break;
                    case "V":
                        sb.Append("Invalid variable reference");
                        break;
                    case "PB":
                        sb.Append("Invalid prgram body. Is there class declaration after the program?");
                        break;
                    case "P":
                        sb.Append("Program does not follow the stucture ClassDecl -> Program -> Functions. ");
                        break;
                    case "FBC1":
                    case "FBC1PP":
                        sb.Append("Invalid variable declaration or statement");
                        break;
                    case "FBC1P":
                    case "FBC1PPP":
                        sb.Append("Invalid variable declaration or assignement statement");
                        break;
                    case "FB":
                        sb.Append("Invalid function block start");
                        break;
                    case "FBE":
                        sb.Append("Invalid function block end");
                        break;
                    case "CD":
                        sb.Append("Invalid class definition start");
                        break;
                    case "CCE":
                        sb.Append("Invalid class definition block end");
                        break;
                    case "I":
                        sb.Append("Invalid array index");
                        break;
                    case "S":
                        sb.Append("Invalid statement");
                        break;
                    case "IN":
                        sb.Append("Invalid nesting sequence. Did you forget an id?");
                        break;
                    case "CC1P":
                        sb.Append("A varaible declaration or a function declaration should follow a varaible declaration");
                        break;
                    case "SB":
                        sb.Append("Statement blocks only accept statements");
                        break;
                    case "FBC2":
                        sb.Append("Invalid statement sequence");
                        break;
                    case "APT":
                        sb.Append("Invalid parameter for function call");
                        break;
                    case "FF":
                        sb.Append("Invalid factor sequence");
                        break;
                    case "FHE":
                        sb.Append("Invalid function declaration parameter format");
                        break;
                    case "FPT":
                        sb.Append("Invalid parameter for function declaration");
                        break;
                    case "FD":
                        sb.Append("Invalid function declaration structure");
                        break;
                    case "OAS":
                        sb.Append("Invalid array sizes sequence");
                        break;
                    case "VE":
                        sb.Append("Invalid variable declaration ending");
                        break;
                    case "CC1":
                        sb.Append("Invalid class declaration body start");
                        break;
                    case "CC1PP":
                    case "CC1PPP":
                        sb.Append("Invalid variable declaration or function declaration");
                        break;
                    default:
                        sb.Append(_derivation_root.Value);
                        break;
                }
            }
            else
            {
                switch (_source_token.Type)
                {
                    case TokenType.T_CLASS:
                        sb.Append("Unexpected class declaration.");
                        break;
                    case TokenType.T_OPENED_BRACKET:
                        sb.Append("Unexpected program body, function body or statement block opening");
                        break;
                    case TokenType.T_CLOSED_BRACKET:
                        sb.Append("Unexpected program body, function body or statement block closing");
                        break;
                    default:
                        sb.Append("Unexpected token");
                        break;
                }
                sb.AppendLine();
            }

            return sb.ToString();
        }

    }
}

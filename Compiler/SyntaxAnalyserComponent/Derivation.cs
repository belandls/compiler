﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : Derivation.cs
 * Description : Takes care of keeping track of derivations and applied productions to get each derivations
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.SyntaxAnalyserComponent
{
    public class Derivation
    {
        //Simple class to replace to ReadOnly Tuple class
        public class Pair<T1, T2>
        {
            public bool IgnoreFurtherGet { get; set; } //Mark this pair to avoid duplicate errors for same root
            public T1 First { get; set; }
            public T2 Second { get; set; }

            public Pair(T1 f, T2 s)
            {
                First = f;
                Second = s;
                IgnoreFurtherGet = false;
            }

        }

        private List<List<Symbol>> _derivations;            //List of all the derivations
        private List<Symbol> _last_derivation;              //The keep track of the last derivation. Simply provides easier access to it instead of from the list 
        private List<Production> _applied_productions;      //List of applied productions to get derivations
        private Stack<Pair<Symbol, List<Symbol>>> _roots;   //Stack of closest root. The closest root is always on top. when a parsing error occurs, the error message is based on the closest dervation root
                                                            //The items in the stack are actually a pair of the LHS and the RHS symbols of a productions.

        public List<List<Symbol>> Derivations
        {
            get
            {
                return _derivations;
            }
        }


        public Symbol CurrentRoot
        {
            get
            {
                //To avoid duplicate error from this root
                if (!_roots.Peek().IgnoreFurtherGet)
                {
                    return _roots.Peek().First;
                }
                return null;
            }
        }

        //MArks the current root as already included in an error
        public bool IgnoreFurtherGetCurrentRoot
        {
            get
            {
                return _roots.Peek().IgnoreFurtherGet;
            }
            set
            {
                _roots.Peek().IgnoreFurtherGet = value;
            }
        }

        public List<Production> AppliedProductions
        {
            get
            {
                return _applied_productions;
            }
        }

        public Derivation(Symbol startingSymbol)
        {
            _derivations = new List<List<Symbol>>();
            _derivations.Add(new List<Symbol>());
            _derivations[0].Add(startingSymbol);
            _last_derivation = _derivations[0];
            _applied_productions = new List<Production>();
            _roots = new Stack<Pair<Symbol, List<Symbol>>>();
        }

        //Applies a production to the current derivation. This will then generate a new derivation.
        public void ApplyProduction(Production p)
        {
            //Copy the last derivation to a new list of symbol (a new derivation basically)
            List<Symbol> temp = new List<Symbol>(_last_derivation);
            //Get the index of the symbol to replace in the derivation
            int replaceIndex = temp.IndexOf(p.LHS);
            
            //Remove the symbol in the derivation and replace it by the production's RHS
            temp.Remove(p.LHS);
            if (!p.IsEpsilon)
            {
                temp.InsertRange(replaceIndex, p.RHS.Where((pc) => pc.InDerivation));
            }
            _last_derivation = temp;
            _derivations.Add(temp);
            _applied_productions.Add(p);

            /* 
             * Ex. CD	->	CLA ID OB CC1             
             * CD is the closest root for CLA, ID, OB, CC1, assuming they are not root themselves
             * If there is an error for any of them, the error will be relative to CD. 
             * Now, the root is replaced by applying another production with multiple symbols in its RHS. 
             * A root is poped (removed) when every symbols have been replaced by a terminal or set of terminals
             */

            //So, if the RHS is a terminal
            if (p.RHS.Count == 1 && ((Symbol)(p.RHS[0])).IsTerminal)
            {
                Symbol s = p.LHS;

                //Remove the non-terminal from the current list of symbol of the current root
                if (_roots.Count > 0)
                {
                    int index = _roots.Peek().Second.IndexOf(s);
                    _roots.Peek().Second.RemoveRange(0, index + 1); //Removes from the beginning up to the element to remove. NEcessary if error skipping occured.
                }
                //If all the symbol have been derived, pop the roots until we find the next root 
                while (_roots.Count > 0 && _roots.Peek().Second.Count == 0)
                {
                    s = _roots.Pop().First;
                }
            }
            else //If the applied production should become the new root
            {
                //Pop the aplpoied production from the current root's symbols
                if (_roots.Count > 0)
                {
                    int index = _roots.Peek().Second.IndexOf(p.LHS);
                    _roots.Peek().Second.RemoveRange(0, index + 1); //Removes from the beginning up to the element to remove. NEcessary if error skipping occured.
                }
                
                //Push the new root on the stack
                _roots.Push(new Pair<Symbol, List<Symbol>>(p.LHS, new List<Symbol>(p.RHS.Where((pc) => pc.InDerivation))));

            }
        }

        //Remove a symbol in the current derivation.
        public void SkipSymbolInLastDerivationStep(Symbol s)
        {
            int replaceIndex = _last_derivation.IndexOf(s);
            if (replaceIndex != -1)
            {
                _last_derivation.Remove(s);
                _last_derivation.Insert(replaceIndex, new Symbol("*PARSE_ERROR*"));
            }
        }

        public string ToString(int index)
        {
            if (index < 0 || index >= _derivations.Count)
            {
                return "";
            }
            StringBuilder sb = new StringBuilder();
            foreach (Symbol s in _derivations[index])
            {
                sb.Append(s.Value).Append(" ");
            }
            return sb.ToString();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (List<Symbol> d in _derivations)
            {
                foreach (Symbol s in d)
                {
                    sb.Append(s.Value).Append(" ");
                }
                sb.AppendLine();
            }
            //foreach (Symbol s in _last_derivation)
            //{
            //    sb.Append(s.Value).Append(" ");
            //}
            sb.AppendLine();
            return sb.ToString();
        }
    }
}

﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : Token.cs
 * Description : Data structure representing a valid or invalid token
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler
{
    public enum TokenType
    {
        T_UNDEFINED,
        T_END_OF_PROGRAM,
        T_ID,
        T_NUM_INTEGER,
        T_NUM_FLOAT,
        T_DOT,
        T_EQUALS,
        T_N_EQUALS,
        T_LESS,
        T_LESS_EQUAL,
        T_GREATER,
        T_GREATER_EQUAL,
        T_ASSIGNMENT,
        T_SEMI_COLUMN,
        T_COMMA,
        T_PLUS,
        T_MINUS,
        T_MULTIPLICATION,
        T_DIVISION,
        T_AND,
        T_OR,
        T_IF,
        T_THEN,
        T_ELSE,
        T_NOT,
        T_RETURN,
        T_FOR,
        T_INT,
        T_FLOAT,
        T_CLASS,
        T_GET,
        T_PUT,
        T_OPENED_PAR,
        T_CLOSED_PAR,
        T_OPENED_BRACKET,
        T_CLOSED_BRACKET,
        T_OPENED_S_BRACKET,
        T_CLOSED_S_BRACKET,
        T_PROGRAM,
        T_COMMENT,
        //Invalid tokens
        T_WHITESPACE,
        T_ERR_INVALID_CHAR,
        T_ERR_LEADING_ZERO,
        T_ERR_TRAILING_ZERO,
        T_ERR_INVALID_FLOAT,
        T_ERR_INVALID_COMMENT_BLOCK
    }

    public class Token
    {
        //Simple structure for the position of the token in the file
        public class TokenPosition
        {
            private int _line_number;
            private int _position_in_line;
            public int LineNumber
            {
                get
                {
                    return _line_number;
                }
            }
            public int PositionInLine
            {
                get
                {
                    return _position_in_line;
                }
            }

            public TokenPosition(int ln, int pil)
            {
                _line_number = ln;
                _position_in_line = pil;
            }
        }

        private static Token _Ws_Token;
        //Static Token representing a Whitespace reused often
        public static Token Whitespace
        {
            get
            {
                if (_Ws_Token == null)
                {
                    _Ws_Token = new Token(TokenType.T_WHITESPACE, null, null);
                }
                return _Ws_Token;
            }
        }

        private TokenType _type;
        private string _value;
        private TokenPosition _position;
        private TokenPosition _end_position; //When on multiple line, used to note the position in the last line of the end of the token. Used to update properly the FileIO
        private string _warning;

        public TokenType Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }
        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }
        public TokenPosition Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }
        public bool IsMultiline
        {
            get
            {
                //Only used in multiline tokens
                return _end_position != null;
            }
        }

        public TokenPosition EndPosition
        {
            get
            {
                return _end_position;
            }
        }
        public bool IsInvalid
        {
            get
            {
                //All error token start by T_ERR
                return _type.ToString().StartsWith("T_ERR");
            }
        }
        public string Warning
        {
            get
            {
                return _warning;
            }
            set
            {
                _warning = value;
            }
        }

        public Token(TokenType type, string value, TokenPosition pos)
        {
            _type = type;
            _value = value;
            _position = pos;
        }
        public Token(TokenType type, string value, TokenPosition pos, string warning) : this(type, value, pos)
        {
            _warning = warning;
        }
        public Token(TokenType type, string value, TokenPosition pos, TokenPosition endPos)
        {
            _type = type;
            _value = value;
            _position = pos;
            _end_position = endPos;
        }
        public Token(TokenType type, string value, TokenPosition pos, TokenPosition endPos, string warning) : this(type,value,pos,endPos)
        {
            _warning = warning;
        }

        public override string ToString()
        {
            return String.Format("{0,-27} ln:{1,-3} col:{2,-3} -> {3}", _type.ToString(), _position.LineNumber, _position.PositionInLine, _value);
        }
    }
}

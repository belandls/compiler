﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : SyntaxAnalyser.cs
 * Description : Tries to parse a stream of tokens while adjusting to errors.
 * 
 * */


using Compiler.SyntaxAnalyserComponent;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Compiler.SemanticAnalyser.SemanticActions;

namespace Compiler
{
    public partial class SyntaxAnalyser
    {
        private Hashtable _table;                       //The parsing table
        private Production _starting_production;        //The starting production
        private List<Token>.Enumerator _token_stream;   //An enumerator to the token stream
        private Stack<Symbol> _parse_stack;             //The temp. parsing stack
        private Derivation _parse_derivations;          //To keep track of the derivations
        private bool _skipping;                         //Flag to know if we are still skipping errors (avoids error duplications)
        private List<SyntaxError> _errors;              //List of all errors detected
        private bool _skip_overflow;                    //In case a errors leads to removing $ from the input stream
        private SemanticAnalyser.SemanticAnalyser _semantic_analyser;

        private Token CurrentToken
        {
            get
            {
                return _token_stream.Current;
            }
        }

        public Derivation ParseDerivations
        {
            get
            {
                return _parse_derivations;
            }
        }

        public List<SyntaxError> Errors
        {
            get
            {
                return _errors;
            }
        }

        public SemanticAnalyser.SemanticAnalyser InternalSemanticAnalyse
        {
            get
            {
                return _semantic_analyser;
            }
        }

        public SyntaxAnalyser(string outName)
        {
            _table = new Hashtable();
            _errors = new List<SyntaxError>();
            _skip_overflow = false;
            _semantic_analyser = new SemanticAnalyser.SemanticAnalyser(outName);
            //Fills the parsing table
            SetupProductions();
        }


        public bool Parse(List<Token> toks)
        {
            Token lastAcceptedToken;
            bool error = false;
            for (int i = 0; i <= 1; i++)
            {
                if (i == 1)
                {
                    _semantic_analyser.CurrentPass = SemanticAnalyser.Pass.SECOND;
                }
                _skipping = false;
                lastAcceptedToken = null;
                _parse_stack = new Stack<Symbol>();
                //Push the end of program and the first production LHS symbol
                _parse_stack.Push(new Symbol("$", false, TokenType.T_END_OF_PROGRAM));
                _parse_stack.Push(new SemanticActionFactory("{99}"));
                _parse_stack.Push(_starting_production.LHS);

                //Initialize the derivation process
                _parse_derivations = new Derivation(_parse_stack.Peek());

                _parse_stack.Push(new SemanticActionFactory("{12}"));

                //Initialize the token enumerator and get the first token
                _token_stream = toks.GetEnumerator();
                MoveToNextToken();
                


                while (_parse_stack.Peek().AssociatedTokenType != TokenType.T_END_OF_PROGRAM && !_skip_overflow)
                {

                    Symbol temp = _parse_stack.Peek(); //Get the top symbol on the parse stack
                    if (temp is SemanticAnalyser.SemanticActions.SemanticActionFactory)
                    {
                        _semantic_analyser.HandleSemanticAction((SemanticAnalyser.SemanticActions.SemanticActionFactory)temp, lastAcceptedToken);
                        _parse_stack.Pop();
                    }
                    else
                    {
                        if (temp.IsTerminal)
                        {
                            if (temp.Equals(_token_stream.Current))
                            {
                                _skipping = false; //If we were skipping, we are not anymore
                                _parse_stack.Pop();
                                lastAcceptedToken = _token_stream.Current;
                                MoveToNextToken();
                            }
                            else
                            {
                                //Will never happen
                                //error = true;
                                //Debugger.Break();
                            }
                        }
                        else
                        {
                            Hashtable cols = (Hashtable)_table[temp.Value];
                            if (cols != null)
                            {
                                Production prod = (Production)cols[_token_stream.Current.Type];
                                //prod will be null if we try to read an empty cell in the table.
                                //Otherwise, it's either a valid production or a dummy POP production
                                if (prod == null || prod.Type != Production.ProductionType.VALID)
                                {
                                    error = true;
                                    SkipError(prod);
                                }
                                else
                                {
                                    _skipping = false; //If we were skipping, we are not anymore
                                    _parse_stack.Pop();
                                    _parse_derivations.ApplyProduction(prod);
                                    //Push the productio in reverse on the stack
                                    if (!prod.IsEpsilon)
                                    {
                                        foreach (Symbol s in prod.RHS.Reverse<Symbol>())
                                        {
                                            _parse_stack.Push(s);
                                        }
                                    }
                                    else
                                    {
                                        foreach (Symbol s in prod.RHS.Reverse<Symbol>())
                                        {
                                            if (s is SemanticActionFactory)
                                            {
                                                _parse_stack.Push(s);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                error = true;
                            }
                        }
                    }

                }
            }
            //Check if end of stack is reached and if the end of program token tried to be skipped
            
            _semantic_analyser.ResoveIdentifiers();
            //_semantic_analyser.CheckForRecursiveMemAlloc();
            _semantic_analyser.ResolveVarAccess();
            _semantic_analyser.ResolveFuncAccess();
            if (_parse_stack.Peek().AssociatedTokenType != TokenType.T_END_OF_PROGRAM || error || _semantic_analyser.Errors.Count > 0)
            {
                return false;
            }
            else
            {
                //_semantic_analyser.Generator.GenerateEndOfProgram();
                return true;
            }

        }

        private void MoveToNextToken()
        {
            //If we are not trying to read pass the end of input
            if (CurrentToken == null || CurrentToken.Type != TokenType.T_END_OF_PROGRAM)
            {
                _token_stream.MoveNext();
                if (CurrentToken != null)
                {
                    //Skip comments since they are irrelevant
                    while (_token_stream.Current.Type == TokenType.T_COMMENT && _token_stream.Current.Type != TokenType.T_END_OF_PROGRAM)
                    {
                        _token_stream.MoveNext();
                    }
                }
            }
            else
            {
                //We tried to read after the end of input
                _skip_overflow = true;
            }
        }

        private void SkipError(Production p)
        {

            //To avoid superfluous error when we skip more than once
            if (!_skipping)
            {
                //To avoid multiple error from the same derivation root (Ex. 2 consecutive errors about a malformed statement)
                if (!_parse_derivations.IgnoreFurtherGetCurrentRoot)
                {
                    _errors.Add(new SyntaxError(CurrentToken, _parse_derivations.CurrentRoot));
                    _parse_derivations.IgnoreFurtherGetCurrentRoot = true;
                }

            }
            if (p != null && p.Type == Production.ProductionType.ERR_POP)
            {
                _parse_derivations.SkipSymbolInLastDerivationStep(_parse_stack.Pop());
            }
            else
            {
                MoveToNextToken();
            }
            _skipping = true;
        }

    }
}

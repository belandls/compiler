﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : IdAnalyser.cs
 * Description : Analyser detecting identifier and extracts reserved words if present
 * 
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Compiler.Analysers
{
    class IdAnalyser : IAnalyser
    {
        private Regex _first_char_regex; //To detect if the analyzer is compatible
        private Regex _id_char_regex;    //To detect an identifier

        public IdAnalyser()
        {
            _first_char_regex = new Regex("[a-zA-Z]");
            _id_char_regex = new Regex("[a-zA-Z][a-zA-Z0-9_]*");
        }

        public bool IsFirstCharacterCompatible(FileIO input)
        {
            Match m = _first_char_regex.Match(input.NextCharacter.ToString());
            return m.Success;
        }

        public Token GetToken(FileIO input)
        {
            Match m = _id_char_regex.Match(input.CurrentLine);

            Token tok;
            Token.TokenPosition pos = new Token.TokenPosition(input.CurrentLineNumber, input.CurrentPositionInLine);

            //If the detected "ID" is a RW, return the apropriate token
            switch (m.Value)
            {
                case "and":
                    tok = new Token(TokenType.T_AND, m.Value, pos);
                    break;
                case "or":
                    tok = new Token(TokenType.T_OR, m.Value, pos);
                    break;
                case "if":
                    tok = new Token(TokenType.T_IF, m.Value, pos);
                    break;
                case "else":
                    tok = new Token(TokenType.T_ELSE, m.Value, pos);
                    break;
                case "then":
                    tok = new Token(TokenType.T_THEN, m.Value, pos);
                    break;
                case "for":
                    tok = new Token(TokenType.T_FOR, m.Value, pos);
                    break;
                case "get":
                    tok = new Token(TokenType.T_GET, m.Value, pos);
                    break;
                case "put":
                    tok = new Token(TokenType.T_PUT, m.Value, pos);
                    break;
                case "class":
                    tok = new Token(TokenType.T_CLASS, m.Value, pos);
                    break;
                case "int":
                    tok = new Token(TokenType.T_INT, m.Value, pos);
                    break;
                case "float":
                    tok = new Token(TokenType.T_FLOAT, m.Value, pos);
                    break;
                case "not":
                    tok = new Token(TokenType.T_NOT, m.Value, pos);
                    break;
                case "return":
                    tok = new Token(TokenType.T_RETURN, m.Value, pos);
                    break;
                case "program":
                    tok = new Token(TokenType.T_PROGRAM, m.Value, pos);
                    break;
                default:
                    //Else it's a normal ID
                    tok = new Token(TokenType.T_ID, m.Value, pos);
                    break;
            }

            return tok;
        }
    }
}

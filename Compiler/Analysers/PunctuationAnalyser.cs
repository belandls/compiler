﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : PunctuationAnalyser.cs
 * Description : Analyser detecting any valid punctuation symbols.
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Compiler.Analysers
{
    class PunctuationAnalyser : IAnalyser
    {
        private Regex _first_char_regex;//To detect if the analyser is compatible

        public PunctuationAnalyser()
        {
            _first_char_regex = new Regex(@"[;,\.\(\)\[\]\{\}]");
        }

        public bool IsFirstCharacterCompatible(FileIO input)
        {
            Match m = _first_char_regex.Match(input.NextCharacter.ToString());
            return m.Success;
        }

        public Token GetToken(FileIO input)
        {
            Token tok = null;
            Token.TokenPosition pos = new Token.TokenPosition(input.CurrentLineNumber, input.CurrentPositionInLine);

            //Since any punctuation is 1 char, is the analyser is compatible, than it has to be one of these cases
            switch (input.NextCharacter)
            {
                case '.':
                    tok = new Token(TokenType.T_DOT, input.NextCharacter.ToString(), pos);
                    break;
                case ';':
                    tok = new Token(TokenType.T_SEMI_COLUMN, input.NextCharacter.ToString(), pos);
                    break;
                case ',':
                    tok = new Token(TokenType.T_COMMA, input.NextCharacter.ToString(), pos);
                    break;
                case '(':
                    tok = new Token(TokenType.T_OPENED_PAR, input.NextCharacter.ToString(), pos);
                    break;
                case ')':
                    tok = new Token(TokenType.T_CLOSED_PAR, input.NextCharacter.ToString(), pos);
                    break;
                case '[':
                    tok = new Token(TokenType.T_OPENED_S_BRACKET, input.NextCharacter.ToString(), pos);
                    break;
                case ']':
                    tok = new Token(TokenType.T_CLOSED_S_BRACKET, input.NextCharacter.ToString(), pos);
                    break;
                case '{':
                    tok = new Token(TokenType.T_OPENED_BRACKET, input.NextCharacter.ToString(), pos);
                    break;
                case '}':
                    tok = new Token(TokenType.T_CLOSED_BRACKET, input.NextCharacter.ToString(), pos);
                    break;
            }

            return tok;
        }
    }
}

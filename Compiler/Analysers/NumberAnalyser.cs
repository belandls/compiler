﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : NumberAnalyser.cs
 * Description : Analyser detecting integer and floats and any invalid numbers.
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Compiler.Analysers
{
    class NumberAnalyser : IAnalyser
    {
        private Regex _first_char_regex;//To detect if the analyzer is compatible
        private Regex _is_float_regex;  //To detect if a number is potentially a float
        private Regex _float_regex;     //To detect a valid float
        private Regex _int_regex;       //To detect a valid integers
        private Regex _zero_int_regex;  //To detect an int with leading eros
        private bool _starting_by_zero; //To know if the number starts by 0

        public NumberAnalyser()
        {
            _first_char_regex = new Regex(@"\d");
            _is_float_regex = new Regex(@"^\d*\.\d*");
            _float_regex = new Regex(@"([1-9]\d*|0)\.(\d*[1-9]|0)");
            //_zero_float_regex = new Regex(@"^(?<!\d)0\.(\d*[1-9]|0)");
            _int_regex = new Regex(@"^[1-9]\d*");
            _zero_int_regex = new Regex(@"^0+\d+");
        }

        public bool IsFirstCharacterCompatible(FileIO input)
        {
            Match m = _first_char_regex.Match(input.NextCharacter.ToString());
            if (m.Success)
            {
                _starting_by_zero = input.NextCharacter == '0';
            }
            return m.Success;
        }

        public Token GetToken(FileIO input)
        {
            Token tok;
            Token.TokenPosition pos = new Token.TokenPosition(input.CurrentLineNumber, input.CurrentPositionInLine);

            string workingLine = input.CurrentLine; //To avoid double substrings
            Match isFloatMatch = _is_float_regex.Match(workingLine);
            
            //First check if the number is potentially a float
            if (isFloatMatch.Success)
            {
                Match floatMatch = _float_regex.Match(isFloatMatch.Value);
                //check that there is a fraction part
                if (floatMatch.Success)
                {
                    //if the potential float has not trailing or leading 0, then the valid float should be the same thing
                    if (floatMatch.Length == isFloatMatch.Length)
                    {
                        tok = new Token(TokenType.T_NUM_FLOAT, floatMatch.Value, pos);
                    }
                    else //There are zeros that should not be there
                    {
                        //if the valid part of the float is at the beginning of the potential float, T_Z
                        if (floatMatch.Index == 0)
                        {
                            tok = new Token(TokenType.T_ERR_TRAILING_ZERO, isFloatMatch.Value, pos, "The float '" + isFloatMatch.Value + "' contains trailing zeros which should be removed.");
                        }
                        else //At lest L_Z
                        {
                            tok = new Token(TokenType.T_ERR_LEADING_ZERO, isFloatMatch.Value, pos, "The float '" + isFloatMatch.Value + "' contains al least leading zeros which should be removed.");
                        }
                    }
                }
                else //No fraction
                {
                    tok = new Token(TokenType.T_ERR_INVALID_FLOAT, isFloatMatch.Value, pos, "The float '"+isFloatMatch.Value+"' does not have a fraction part.");
                }
            }
            else //if not a float, it has to be an integer
            {
                if (_starting_by_zero)
                {
                    //If it starts with 0, be sure to check it's only '0'
                    Match zeroIntMatch = _zero_int_regex.Match(workingLine);
                    if (zeroIntMatch.Success)
                    {
                        tok = new Token(TokenType.T_ERR_LEADING_ZERO, zeroIntMatch.Value, pos, "The integer '" + zeroIntMatch.Value + "' contains leading zeros that should be removed");
                    }
                    else
                    {
                        tok = new Token(TokenType.T_NUM_INTEGER, "0", pos);
                    }
                }
                else
                {
                    //if it's not a float and does not start with a 0, then since the ana. is compatible, for sure it's a valid int
                    Match intMatch = _int_regex.Match(workingLine);
                    tok = new Token(TokenType.T_NUM_INTEGER, intMatch.Value, pos);
                }
            }


            return tok;
        }
    }
}

﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : CommentAnalyser.cs
 * Description : Analyser detecting single and multi line comments. Also detects invalid block comments.
 * 
 * */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Compiler.Analysers
{
    class CommentAnalyser : IAnalyser
    {
        private Regex _first_char_regex;        //To detect if the first character is compatible
        private Regex _single_line_regex;       //To detect a single line comment
        private Regex _multi_line_beg_regex;    //To detect a comment block beginning
        private Regex _multi_line_end_regex;    //To detect a comment block ending

        public CommentAnalyser()
        {
            _first_char_regex = new Regex(@"^(\/\/|\/\*)");
            _single_line_regex = new Regex(@"^(\/\/.*|\/\*.*\*\/)");
            _multi_line_beg_regex = new Regex(@"\/\*");
            _multi_line_end_regex = new Regex(@"\*\/");
            //_multi_line_end_regex = new Regex(@"\/\*(.|\n)*\*\/");
        }

        public bool IsFirstCharacterCompatible(FileIO input)
        {
            if (input.NextCharacter == '/'){
                Match m = _first_char_regex.Match(input.CurrentLine);
                return m.Success;
            }
            return false;
        }

        public Token GetToken(FileIO input)
        {
            Token tok = null;
            Token.TokenPosition pos = new Token.TokenPosition(input.CurrentLineNumber, input.CurrentPositionInLine);
            Token.TokenPosition multilineEndPos = null; //Only used for block comments to update the FileIO properly

            Match slMatch = _single_line_regex.Match(input.CurrentLine);
            //Check if we have a single line comment
            if (slMatch.Success)
            {
                //Take the rest of the current line
                tok = new Token(TokenType.T_COMMENT, slMatch.Value, pos);
            }
            else
            {
                //Find the whole block comment
                StringBuilder sb = new StringBuilder();
                int depth = 0; //To allow nested blocks
                do
                {
                    var mlBegMatches = _multi_line_beg_regex.Matches(input.CurrentLine);
                    //each time one or more begining are detected, increase the depth
                    if (mlBegMatches.Count > 0 && depth == 0)
                    {
                        depth += 1;
                    }
                    var mlEndMatches = _multi_line_end_regex.Matches(input.CurrentLine);
                    //each time one or more end iare detected, decrease the depth
                    if (mlEndMatches.Count > 0)
                    {
                        depth -= 1;
                    }
                    //We detected a complete comment block
                    if (depth == 0)
                    {
                        Match lastMatch = mlEndMatches[0];
                        sb.Append(input.CurrentLine.Substring(0, lastMatch.Index + lastMatch.Length));
                        multilineEndPos = new Token.TokenPosition(input.CurrentLineNumber, lastMatch.Index + lastMatch.Length);
                    }
                    else //Continue looking on the next line and keep track of the text
                    {
                        sb.Append(input.CurrentLine);
                        input.GetNextWorkingLine();
                    }

                } while (depth != 0 && !input.EOF); //The end is not found or the file is not finished
                ////Find the whole block comment
                //StringBuilder sb = new StringBuilder();
                //int depth = 0; //To allow nested blocks
                //do
                //{
                //    var mlBegMatches = _multi_line_beg_regex.Matches(input.CurrentLine);
                //    //each time one or more begining are detected, increase the depth
                //    if (mlBegMatches.Count > 0)
                //    {
                //        depth += mlBegMatches.Count;
                //    }
                //    var mlEndMatches = _multi_line_end_regex.Matches(input.CurrentLine);
                //    //each time one or more end iare detected, decrease the depth
                //    if (mlEndMatches.Count > 0)
                //    {
                //        depth -= mlEndMatches.Count;
                //    }
                //    //We detected a complete comment block
                //    if (depth == 0)
                //    {
                //        Match lastMatch = mlEndMatches[mlEndMatches.Count - 1];
                //        sb.Append(input.CurrentLine.Substring(0,lastMatch.Index + lastMatch.Length));
                //        multilineEndPos = new Token.TokenPosition(input.CurrentLineNumber,lastMatch.Index + lastMatch.Length); 
                //    }
                //    else //Continue looking on the next line and keep track of the text
                //    {
                //        sb.Append(input.CurrentLine);
                //        input.GetNextWorkingLine();
                //    }

                //} while (depth != 0 && !input.EOF); //The end is not found or the file is not finished

                //No ending are found before the end of the file
                if (input.EOF && depth != 0)
                {
                    tok = new Token(TokenType.T_ERR_INVALID_COMMENT_BLOCK, sb.ToString(), pos, "The comment block is not closed. Are nested blocks also closed?");
                }
                else
                {
                    tok = new Token(TokenType.T_COMMENT, sb.ToString(), pos, multilineEndPos);
                }
            }

            return tok;

        }
    }
}

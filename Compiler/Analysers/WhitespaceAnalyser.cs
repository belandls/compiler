﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : WhitespaceAnalyser.cs
 * Description : Analyser detecting whitespace characters. Simply needed to keep track of in-line position of other tokens.
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.Analysers
{
    class WhitespaceAnalyser : IAnalyser
    {
        public bool IsFirstCharacterCompatible(FileIO input)
        {
            return input.NextCharacter == ' ' || input.NextCharacter == '\t';
        }

        public Token GetToken(FileIO input)
        {
            return Token.Whitespace;
        }
    }
}

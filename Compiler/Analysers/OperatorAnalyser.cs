﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : OperatorAnalyser.cs
 * Description : Analyser detecting operators (including 2 characters ops.)
 * 
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Compiler.Analysers
{
    class OperatorAnalyser : IAnalyser
    {
        private Regex _first_char_regex;//To detect if the analyzer is compatible
        private Regex _n_eq_regex;      //To detect a not equal operator
        private Regex _equ_regex;       //To detect an equals operator
        private Regex _le_ge_regex;     //To detect a greater or equals or a less or equals operator


        public OperatorAnalyser()
        {
            _first_char_regex = new Regex(@"[=<>\+\*\-\/]");
            _equ_regex = new Regex(@"^==");
            _n_eq_regex = new Regex(@"^<>");
            _le_ge_regex = new Regex(@"^[<>]=");
        }

        public bool IsFirstCharacterCompatible(FileIO input)
        {
            Match m = _first_char_regex.Match(input.NextCharacter.ToString());
            return m.Success;
        }

        public Token GetToken(FileIO input)
        {
            Token tok = null;
            Token.TokenPosition pos = new Token.TokenPosition(input.CurrentLineNumber, input.CurrentPositionInLine);

            //Depending on the possible operators from the next char, 
            //try to find if it's a 2 char op, else return single op.
            switch (input.NextCharacter)
            {
                case '=':
                    Match m1 = _equ_regex.Match(input.CurrentLine);
                    if (m1.Success)
                    {
                        tok = new Token(TokenType.T_EQUALS, m1.Value, pos);
                    }
                    else
                    {
                        tok = new Token(TokenType.T_ASSIGNMENT, "=", pos);
                    }
                    break;
                case '<':
                    Match m2 = _le_ge_regex.Match(input.CurrentLine);
                    if (m2.Success)
                    {
                        tok = new Token(TokenType.T_LESS_EQUAL, m2.Value, pos);
                    }
                    else
                    {
                        m2 = _n_eq_regex.Match(input.CurrentLine);
                        if (m2.Success)
                        {
                            tok = new Token(TokenType.T_N_EQUALS, m2.Value, pos);
                        }
                        else
                        {
                            tok = new Token(TokenType.T_LESS, "<", pos);
                        }
                    }
                    break;
                case '>':
                    Match m3 = _le_ge_regex.Match(input.CurrentLine);
                    if (m3.Success)
                    {
                        tok = new Token(TokenType.T_GREATER_EQUAL, m3.Value, pos);
                    }
                    else
                    {
                        tok = new Token(TokenType.T_GREATER, ">", pos);
                    }
                    break;
                case '+':
                    tok = new Token(TokenType.T_PLUS, "+", pos);
                    break;
                case '-':
                    tok = new Token(TokenType.T_MINUS, "-", pos);
                    break;
                case '/':
                    //The analyzers order makes it so this is a valid assumption because the next character will for sure be something else than a second '/'
                    tok = new Token(TokenType.T_DIVISION, "/", pos);
                    break;
                case '*':
                    tok = new Token(TokenType.T_MULTIPLICATION, "*", pos);
                    break;
            }

            return tok;
        }
    }
}

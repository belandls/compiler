﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : IAnalyser.cs
 * Description : Common interface for all the analysers
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.Analysers
{
    interface IAnalyser
    {
        bool IsFirstCharacterCompatible(FileIO input);
        Token GetToken(FileIO input);
    }
}

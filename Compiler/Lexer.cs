﻿/*
 * Author : Samuel Beland-Leblanc - 27185642
 * File : Lexer.cs
 * Description : Core logic of the lexer. Extracts the tokens from a file.
 * 
 * */

using Compiler.Analysers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler
{
    public class Lexer
    {

        private FileIO _input;

        public Lexer(FileIO input)
        {
            _input = input;
        }

        

        public List<List<Token>> ExtractTokens()
        {
            //A list of lists to keep track of the token
            // 0 -> valid tokens
            // 1 -> invalid tokens
            List<List<Token>> tokens = new List<List<Token>>();
            tokens.Add(new List<Token>());
            tokens.Add(new List<Token>());

            //The order matters! Comments must be checked before operators for example.
            var analysers = new List<IAnalyser>();
            analysers.Add(new WhitespaceAnalyser());
            analysers.Add(new CommentAnalyser());
            analysers.Add(new IdAnalyser());
            analysers.Add(new NumberAnalyser());
            analysers.Add(new OperatorAnalyser());
            analysers.Add(new PunctuationAnalyser());

            while (!_input.EOF)
            {
                int i;
                //Tries to find a compatible analyzer in the order they were added to the list
                for (i = 0; i < analysers.Count; i++)
                {
                    IAnalyser ia = analysers[i];
                    if (ia.IsFirstCharacterCompatible(_input))
                    {
                        var t = ia.GetToken(_input);
                        //Adjust the current position in the file
                        _input.UpdateCurrentLine(t);
                        if (t.IsInvalid){
                            tokens[1].Add(t);
                        }else if (t.Type != TokenType.T_WHITESPACE){ //Ignore whitespaces
                            tokens[0].Add(t);
                        }
                        break;
                    }
                }
                //If no analyzer is compatible, than the character is invlaid
                if (i == analysers.Count)
                {
                    Token t = new Token(TokenType.T_ERR_INVALID_CHAR, _input.NextCharacter.ToString(), new Token.TokenPosition(_input.CurrentLineNumber, _input.CurrentPositionInLine), "The character '"+_input.NextCharacter+"' is either not recognized or should not exist by itself");
                    tokens[1].Add(t);
                    _input.UpdateCurrentLine(t);
                }
            }
            tokens[0].Add(new Token(TokenType.T_END_OF_PROGRAM, "$", new Token.TokenPosition(_input.CurrentLineNumber,_input.CurrentPositionInLine)));
            return tokens;
        }

    }
}
